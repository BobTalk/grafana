/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:28
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 17:09:26
 * @FilePath: /grafana/public/app/plugins/panel/news/module.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { isString } from 'lodash';
import { PanelPlugin } from '@grafana/data';
import { NewsPanel } from './NewsPanel';
import { PanelOptions, defaultPanelOptions } from './models.gen';
import { DEFAULT_FEED_URL, PROXY_PREFIX } from './constants';

export const plugin = new PanelPlugin<PanelOptions>(NewsPanel).setPanelOptions((builder) => {
  builder
    .addTextInput({
      path: 'feedUrl',
      name: 'URL地址',
      description: '只支持RSS提要格式（不支持Atom）。',
      settings: {
        placeholder: DEFAULT_FEED_URL,
      },
      defaultValue: defaultPanelOptions.feedUrl,
    })
    .addBooleanSwitch({
      path: 'showImage',
      name: '显示图像',
      description: '控制新闻项目社交（og:image）图像是否显示在文本内容上方',
      showIf: (currentConfig: PanelOptions) => {
        return isString(currentConfig.feedUrl) && !currentConfig.feedUrl.startsWith(PROXY_PREFIX);
      },
      defaultValue: defaultPanelOptions.showImage,
    })
    .addBooleanSwitch({
      path: 'useProxy',
      name: '使用代理',
      description: '如果提要无法连接，请考虑使用CORS代理',
      showIf: (currentConfig: PanelOptions) => {
        return isString(currentConfig.feedUrl) && !currentConfig.feedUrl.startsWith(PROXY_PREFIX);
      },
      defaultValue: defaultPanelOptions.useProxy,
    });
});
