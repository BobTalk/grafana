/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:28
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 12:00:10
 * @FilePath: /grafana/public/app/plugins/datasource/jaeger/components/QueryEditor.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { css } from '@emotion/css';
import { QueryEditorProps } from '@grafana/data';
import { selectors } from '@grafana/e2e-selectors';
import { InlineField, InlineFieldRow, Input, RadioButtonGroup } from '@grafana/ui';
import React from 'react';
import { JaegerDatasource } from '../datasource';
import { JaegerQuery, JaegerQueryType } from '../types';
import { SearchForm } from './SearchForm';

type Props = QueryEditorProps<JaegerDatasource, JaegerQuery>;

export function QueryEditor({ datasource, query, onChange }: Props) {
  return (
    <div className={css({ width: '50%' })}>
      <InlineFieldRow>
        <InlineField label="Query type">
          <RadioButtonGroup<JaegerQueryType>
            options={[
              { value: 'search', label: '搜索' },
              { value: undefined, label: 'TraceID' },
            ]}
            value={query.queryType}
            onChange={(v) =>
              onChange({
                ...query,
                queryType: v,
              })
            }
            size="md"
          />
        </InlineField>
      </InlineFieldRow>
      {query.queryType === 'search' ? (
        <SearchForm datasource={datasource} query={query} onChange={onChange} />
      ) : (
        <InlineFieldRow>
          <InlineField label="Trace ID" labelWidth={21} grow>
            <Input
              aria-label={selectors.components.DataSource.Jaeger.traceIDInput}
              placeholder="Eg. 4050b8060d659e52"
              value={query.query || ''}
              onChange={(v) =>
                onChange({
                  ...query,
                  query: v.currentTarget.value,
                })
              }
            />
          </InlineField>
        </InlineFieldRow>
      )}
    </div>
  );
}
