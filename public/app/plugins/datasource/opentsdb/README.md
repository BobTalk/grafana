# OpenTSDB数据源-本机插件

Grafana附带了对OpenTSDB的**内置**支持，OpenTSDB是一个可扩展的分布式时间序列数据库。

点击此处了解更多:

[http://docs.grafana.org/datasources/opentsdb/](http://docs.grafana.org/datasources/opentsdb/)
