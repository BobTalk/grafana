/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:28
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 14:24:04
 * @FilePath: /grafana/public/app/plugins/datasource/prometheus/components/PromExemplarField.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { GrafanaTheme } from '@grafana/data';
import { IconButton, InlineLabel, Tooltip, useStyles } from '@grafana/ui';
import { css, cx } from '@emotion/css';
import React, { useEffect, useState } from 'react';
import { PrometheusDatasource } from '../datasource';

interface Props {
  isEnabled: boolean;
  onChange: (isEnabled: boolean) => void;
  datasource: PrometheusDatasource;
}

export function PromExemplarField({ datasource, onChange, isEnabled }: Props) {
  const [error, setError] = useState<string>();
  const styles = useStyles(getStyles);

  useEffect(() => {
    const subscription = datasource.exemplarErrors.subscribe((err) => {
      setError(err);
    });
    return () => {
      subscription.unsubscribe();
    };
  }, [datasource]);

  const iconButtonStyles = cx(
    {
      [styles.activeIcon]: isEnabled,
    },
    styles.eyeIcon
  );

  return (
    <InlineLabel width="auto">
      <Tooltip content={error ?? ''}>
        <div className={styles.iconWrapper}>
          Exemplars
          <IconButton
            name="eye"
            tooltip={isEnabled ? '禁用带有示例的查询' : '启用带有示例的查询'}
            disabled={!!error}
            className={iconButtonStyles}
            onClick={() => {
              onChange(!isEnabled);
            }}
          />
        </div>
      </Tooltip>
    </InlineLabel>
  );
}

function getStyles(theme: GrafanaTheme) {
  return {
    eyeIcon: css`
      margin-left: ${theme.spacing.md};
    `,
    activeIcon: css`
      color: ${theme.palette.blue95};
    `,
    iconWrapper: css`
      display: flex;
      align-items: center;
    `,
  };
}
