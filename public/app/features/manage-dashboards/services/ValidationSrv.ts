/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-30 22:05:49
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-31 00:26:56
 * @FilePath: /grafana/public/app/features/manage-dashboards/services/ValidationSrv.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import coreModule from 'app/core/core_module';
import { backendSrv } from 'app/core/services/backend_srv';

const hitTypes = {
  FOLDER: 'dash-folder',
  DASHBOARD: 'dash-db',
};

export class ValidationSrv {
  rootName = 'general';

  validateNewDashboardName(folderId: any, name: string) {
    return this.validate(folderId, name, '具有相同名称的仪表板或文件夹已存在');
  }

  validateNewFolderName(name?: string) {
    return this.validate(0, name, '常规文件夹中已存在同名的文件夹或仪表板');
  }

  private async validate(folderId: any, name: string | undefined, existingErrorMessage: string) {
    name = (name || '').trim();
    const nameLowerCased = name.toLowerCase();

    if (name.length === 0) {
      throw {
        type: 'REQUIRED',
        message: '姓名是必填项',
      };
    }

    if (folderId === 0 && nameLowerCased === this.rootName) {
      throw {
        type: 'EXISTING',
        message: '这是一个保留名称，不能用于文件夹。',
      };
    }

    const promises = [];
    promises.push(backendSrv.search({ type: hitTypes.FOLDER, folderIds: [folderId], query: name }));
    promises.push(backendSrv.search({ type: hitTypes.DASHBOARD, folderIds: [folderId], query: name }));

    const res = await Promise.all(promises);
    let hits: any[] = [];

    if (res.length > 0 && res[0].length > 0) {
      hits = res[0];
    }

    if (res.length > 1 && res[1].length > 0) {
      hits = hits.concat(res[1]);
    }

    for (const hit of hits) {
      if (nameLowerCased === hit.title.toLowerCase()) {
        throw {
          type: 'EXISTING',
          message: existingErrorMessage,
        };
      }
    }

    return;
  }
}

const validationSrv = new ValidationSrv();

export default validationSrv;

coreModule.service('validationSrv', ValidationSrv);
