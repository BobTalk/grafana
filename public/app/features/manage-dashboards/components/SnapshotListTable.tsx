/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 20:46:01
 * @FilePath: /grafana/public/app/features/manage-dashboards/components/SnapshotListTable.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FC, useState, useCallback } from 'react';
import { ConfirmModal, Button, LinkButton } from '@grafana/ui';
import { getBackendSrv, locationService } from '@grafana/runtime';
import { Snapshot } from '../types';
import useAsync from 'react-use/lib/useAsync';

export function getSnapshots() {
  return getBackendSrv()
    .get('/api/dashboard/snapshots')
    .then((result: Snapshot[]) => {
      return result.map((snapshot) => ({
        ...snapshot,
        url: `/dashboard/snapshot/${snapshot.key}`,
      }));
    });
}
export const SnapshotListTable: FC = () => {
  const [snapshots, setSnapshots] = useState<Snapshot[]>([]);
  const [removeSnapshot, setRemoveSnapshot] = useState<Snapshot | undefined>();
  const currentPath = locationService.getLocation().pathname;
  const fullUrl = window.location.href;
  const baseUrl = fullUrl.substr(0, fullUrl.indexOf(currentPath));

  useAsync(async () => {
    const response = await getSnapshots();
    setSnapshots(response);
  }, [setSnapshots]);

  const doRemoveSnapshot = useCallback(
    async (snapshot: Snapshot) => {
      const filteredSnapshots = snapshots.filter((ss) => ss.key !== snapshot.key);
      setSnapshots(filteredSnapshots);
      await getBackendSrv()
        .delete(`/api/snapshots/${snapshot.key}`)
        .catch(() => {
          setSnapshots(snapshots);
        });
    },
    [snapshots]
  );

  return (
    <div>
      <table className="filter-table">
        <thead>
          <tr>
            <th>
              <strong>名称</strong>
            </th>
            <th>
              <strong>快照地址</strong>
            </th>
            <th style={{ width: '70px' }}></th>
            <th style={{ width: '30px' }}></th>
            <th style={{ width: '25px' }}></th>
          </tr>
        </thead>
        <tbody>
          {snapshots.map((snapshot) => {
            const url = snapshot.externalUrl || snapshot.url;
            const fullUrl = snapshot.externalUrl || `${baseUrl}${snapshot.url}`;
            return (
              <tr key={snapshot.key}>
                <td>
                  <a href={url}>{snapshot.name}</a>
                </td>
                <td>
                  <a href={url}>{fullUrl}</a>
                </td>
                <td>{snapshot.external && <span className="query-keyword">External</span>}</td>
                <td className="text-center">
                  <LinkButton href={url} variant="secondary" size="sm" icon="eye">
                    预览
                  </LinkButton>
                </td>
                <td className="text-right">
                  <Button variant="destructive" size="sm" icon="times" onClick={() => setRemoveSnapshot(snapshot)} />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <ConfirmModal
        isOpen={!!removeSnapshot}
        icon="trash-alt"
        title="删除"
        body={`你确定要删除 '${removeSnapshot?.name}'?`}
        confirmText="确定"
        dismissText="取消"
        onDismiss={() => setRemoveSnapshot(undefined)}
        onConfirm={() => {
          doRemoveSnapshot(removeSnapshot!);
          setRemoveSnapshot(undefined);
        }}
      />
    </div>
  );
};
