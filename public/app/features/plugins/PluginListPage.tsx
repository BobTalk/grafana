/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:28
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 13:50:10
 * @FilePath: /grafana/public/app/features/plugins/PluginListPage.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React from 'react';
import { hot } from 'react-hot-loader';
import { connect, ConnectedProps } from 'react-redux';
import Page from 'app/core/components/Page/Page';
import PageActionBar from 'app/core/components/PageActionBar/PageActionBar';
import PluginList from './PluginList';
import { loadPlugins } from './state/actions';
import { getNavModel } from 'app/core/selectors/navModel';
import { getPlugins, getPluginsSearchQuery } from './state/selectors';
import { StoreState } from 'app/types';
import { setPluginsSearchQuery } from './state/reducers';
import { useAsync } from 'react-use';
import { selectors } from '@grafana/e2e-selectors';
import { PluginsErrorsInfo } from './PluginsErrorsInfo';
import { config } from '@grafana/runtime';

const mapStateToProps = (state: StoreState) => ({
  navModel: getNavModel(state.navIndex, 'plugins'),
  plugins: getPlugins(state.plugins),
  searchQuery: getPluginsSearchQuery(state.plugins),
  hasFetched: state.plugins.hasFetched,
});

const mapDispatchToProps = {
  loadPlugins,
  setPluginsSearchQuery,
};

const connector = connect(mapStateToProps, mapDispatchToProps);
export type Props = ConnectedProps<typeof connector>;

export const PluginListPage: React.FC<Props> = ({
  hasFetched,
  navModel,
  plugins,
  setPluginsSearchQuery,
  searchQuery,
  loadPlugins,
}) => {
  useAsync(async () => {
    loadPlugins();
  }, [loadPlugins]);

  let actionTarget: string | undefined = '_blank';
  const linkButton = {
    href: 'https://grafana.com/plugins?utm_source=grafana_plugin_list',
    title: '在Grafana.com上查找更多插件',
  };

  if (config.pluginAdminEnabled) {
    linkButton.href = '/a/grafana-plugin-admin-app/';
    linkButton.title = '安装和管理插件';
    actionTarget = undefined;
  }

  return (
    <Page navModel={navModel} aria-label={selectors.pages.PluginsList.page}>
      <Page.Contents isLoading={!hasFetched}>
        <>
          <PageActionBar
            searchQuery={searchQuery}
            setSearchQuery={(query) => setPluginsSearchQuery(query)}
            linkButton={linkButton}
            placeholder="按姓名、作者、描述或类型搜索"
            target={actionTarget}
          />
          <PluginsErrorsInfo />
          {hasFetched && plugins && <PluginList plugins={plugins} />}
        </>
      </Page.Contents>
    </Page>
  );
};

export default hot(module)(connect(mapStateToProps, mapDispatchToProps)(PluginListPage));
