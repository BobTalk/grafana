/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:28
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 13:41:11
 * @FilePath: /grafana/public/app/features/users/InviteeRow.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Invitee } from 'app/types';
import { revokeInvite } from './state/actions';
import { Button, ClipboardButton } from '@grafana/ui';

export interface Props {
  invitee: Invitee;
  revokeInvite: typeof revokeInvite;
}

class InviteeRow extends PureComponent<Props> {
  render() {
    const { invitee, revokeInvite } = this.props;
    return (
      <tr>
        <td>{invitee.email}</td>
        <td>{invitee.name}</td>
        <td className="text-right">
          <ClipboardButton variant="secondary" size="sm" getText={() => invitee.url}>
            {/* Copy Invite */}
            复制请求
          </ClipboardButton>
          &nbsp;
        </td>
        <td>
          <Button variant="destructive" size="sm" icon="times" onClick={() => revokeInvite(invitee.code)} />
        </td>
      </tr>
    );
  }
}

const mapDispatchToProps = {
  revokeInvite,
};

export default connect(() => {
  return {};
}, mapDispatchToProps)(InviteeRow);
