/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:28
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 13:40:01
 * @FilePath: /grafana/public/app/features/users/UsersActionBar.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { setUsersSearchQuery } from './state/reducers';
import { getInviteesCount, getUsersSearchQuery } from './state/selectors';
import { FilterInput } from 'app/core/components/FilterInput/FilterInput';
import { RadioButtonGroup, LinkButton } from '@grafana/ui';
import { contextSrv } from 'app/core/core';
import { AccessControlAction } from 'app/types';

export interface Props {
  searchQuery: string;
  setUsersSearchQuery: typeof setUsersSearchQuery;
  onShowInvites: () => void;
  pendingInvitesCount: number;
  canInvite: boolean;
  showInvites: boolean;
  externalUserMngLinkUrl: string;
  externalUserMngLinkName: string;
}

export class UsersActionBar extends PureComponent<Props> {
  render() {
    const {
      canInvite,
      externalUserMngLinkName,
      externalUserMngLinkUrl,
      searchQuery,
      pendingInvitesCount,
      setUsersSearchQuery,
      onShowInvites,
      showInvites,
    } = this.props;
    const options = [
      { label: '用户', value: 'users' },
      { label: `Pending Invites (${pendingInvitesCount})`, value: 'invites' },
    ];
    const canAddToOrg = contextSrv.hasPermission(AccessControlAction.OrgUsersAdd);

    return (
      <div className="page-action-bar">
        <div className="gf-form gf-form--grow">
          <FilterInput
            value={searchQuery}
            onChange={setUsersSearchQuery}
            placeholder="按登录名、电子邮件或姓名搜索用户"
          />
        </div>
        {pendingInvitesCount > 0 && (
          <div style={{ marginLeft: '1rem' }}>
            <RadioButtonGroup value={showInvites ? 'invites' : 'users'} options={options} onChange={onShowInvites} />
          </div>
        )}
        {canInvite && canAddToOrg && <LinkButton href="org/users/invite">执行</LinkButton>}
        {externalUserMngLinkUrl && (
          <LinkButton href={externalUserMngLinkUrl} target="_blank" rel="noopener">
            {externalUserMngLinkName}
          </LinkButton>
        )}
      </div>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    searchQuery: getUsersSearchQuery(state.users),
    pendingInvitesCount: getInviteesCount(state.users),
    externalUserMngLinkName: state.users.externalUserMngLinkName,
    externalUserMngLinkUrl: state.users.externalUserMngLinkUrl,
    canInvite: state.users.canInvite,
  };
}

const mapDispatchToProps = {
  setUsersSearchQuery,
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersActionBar);
