/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:28
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 20:54:43
 * @FilePath: /grafana/public/app/features/playlist/PlaylistForm.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FC } from 'react';
import { config } from '@grafana/runtime';
import { Button, Field, Form, HorizontalGroup, Input, LinkButton } from '@grafana/ui';
import { selectors } from '@grafana/e2e-selectors';

import { Playlist } from './types';
import { DashboardPicker } from '../../core/components/Select/DashboardPicker';
import { TagFilter } from '../../core/components/TagFilter/TagFilter';
import { SearchSrv } from '../../core/services/search_srv';
import { usePlaylistItems } from './usePlaylistItems';
import { PlaylistTable } from './PlaylistTable';

interface PlaylistFormProps {
  onSubmit: (playlist: Playlist) => void;
  playlist: Playlist;
}

const searchSrv = new SearchSrv();

export const PlaylistForm: FC<PlaylistFormProps> = ({ onSubmit, playlist }) => {
  const { name, interval, items: propItems } = playlist;
  const { items, addById, addByTag, deleteItem, moveDown, moveUp } = usePlaylistItems(propItems);
  return (
    <>
      <Form onSubmit={(list: Playlist) => onSubmit({ ...list, items })} validateOn={'onBlur'}>
        {({ register, errors }) => {
          const isDisabled = items.length === 0 || Object.keys(errors).length > 0;
          return (
            <>
              <Field label="名称" invalid={!!errors.name} error={errors?.name?.message}>
                <Input
                  type="text"
                  {...register('name', { required: '名称为必填项' })}
                  placeholder="请输入"
                  defaultValue={name}
                  aria-label={selectors.pages.PlaylistForm.name}
                />
              </Field>
              <Field label="间隔" invalid={!!errors.interval} error={errors?.interval?.message}>
                <Input
                  type="text"
                  {...register('interval', { required: '间隔为必填项' })}
                  placeholder="5m"
                  defaultValue={interval ?? '5m'}
                  aria-label={selectors.pages.PlaylistForm.interval}
                />
              </Field>

              <PlaylistTable items={items} onMoveUp={moveUp} onMoveDown={moveDown} onDelete={deleteItem} />

              <div className="gf-form-group">
                <h3 className="page-headering">添加仪表板</h3>

                <Field label="按标题添加">
                  <DashboardPicker onChange={addById} isClearable />
                </Field>

                <Field label="按标记添加">
                  <TagFilter
                    isClearable
                    tags={[]}
                    hideValues
                    tagOptions={searchSrv.getDashboardTags}
                    onChange={addByTag}
                    placeholder={'请选择'}
                  />
                </Field>
              </div>

              <HorizontalGroup>
                <Button variant="primary" disabled={isDisabled}>
                  保存
                </Button>
                <LinkButton variant="secondary" href={`${config.appSubUrl}/playlists`}>
                  取消
                </LinkButton>
              </HorizontalGroup>
            </>
          );
        }}
      </Form>
    </>
  );
};
