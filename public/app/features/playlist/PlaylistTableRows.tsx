/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:28
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 20:52:39
 * @FilePath: /grafana/public/app/features/playlist/PlaylistTableRows.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FC } from 'react';

import { PlaylistTableRow } from './PlaylistTableRow';
import { PlaylistItem } from './types';

interface PlaylistTableRowsProps {
  items: PlaylistItem[];
  onMoveUp: (item: PlaylistItem) => void;
  onMoveDown: (item: PlaylistItem) => void;
  onDelete: (item: PlaylistItem) => void;
}

export const PlaylistTableRows: FC<PlaylistTableRowsProps> = ({ items, onMoveUp, onMoveDown, onDelete }) => {
  if (items.length === 0) {
    return (
      <tr>
        <td>
          <em>播放列表为空,在下面可添加仪表板</em>
        </td>
      </tr>
    );
  }

  return (
    <>
      {items.map((item, index) => {
        const first = index === 0;
        const last = index === items.length - 1;
        return (
          <PlaylistTableRow
            first={first}
            last={last}
            item={item}
            onDelete={onDelete}
            onMoveDown={onMoveDown}
            onMoveUp={onMoveUp}
            key={item.title}
          />
        );
      })}
    </>
  );
};
