/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 17:25:53
 * @FilePath: /grafana/public/app/features/alerting/unified/components/rule-editor/GrafanaAlertStatePicker.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { SelectableValue } from '@grafana/data';
import { Select } from '@grafana/ui';
import { SelectBaseProps } from '@grafana/ui/src/components/Select/types';
import { GrafanaAlertStateDecision } from 'app/types/unified-alerting-dto';
import React, { FC, useMemo } from 'react';

type Props = Omit<SelectBaseProps<GrafanaAlertStateDecision>, 'options'> & {
  includeNoData: boolean;
};

const options: SelectableValue[] = [
  { value: GrafanaAlertStateDecision.Alerting, label: '警报' },
  { value: GrafanaAlertStateDecision.NoData, label: '暂无数据' },
  { value: GrafanaAlertStateDecision.OK, label: 'OK' },
];

export const GrafanaAlertStatePicker: FC<Props> = ({ includeNoData, ...props }) => {
  const opts = useMemo(() => {
    if (includeNoData) {
      return options;
    }
    return options.filter((opt) => opt.value !== GrafanaAlertStateDecision.NoData);
  }, [includeNoData]);
  return <Select options={opts} {...props} />;
};
