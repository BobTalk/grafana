/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 13:16:22
 * @FilePath: /grafana/public/app/features/alerting/components/NotificationSettings.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FC } from 'react';
import { Checkbox, CollapsableSection, Field, InfoBox, Input } from '@grafana/ui';
import { NotificationSettingsProps } from './NotificationChannelForm';

interface Props extends NotificationSettingsProps {
  imageRendererAvailable: boolean;
}

export const NotificationSettings: FC<Props> = ({ currentFormValues, imageRendererAvailable, register }) => {
  return (
    <CollapsableSection label="通知设置" isOpen={false}>
      <Field>
        <Checkbox {...register('isDefault')} label="是否执行" description="将此通知用于所有警报" />
      </Field>
      <Field>
        <Checkbox {...register('settings.uploadImage')} label="包括图像" description="捕获图像并将其包含在通知中" />
      </Field>
      {currentFormValues.uploadImage && !imageRendererAvailable && (
        <InfoBox title="没有可用/安装的图像渲染器">
          Grafana找不到用于捕获通知图像的图像渲染器，请确保已安装Grafana图像渲染器插件，请联系您的Grafana管理员以安装插件。
        </InfoBox>
      )}
      <Field>
        <Checkbox
          {...register('disableResolveMessage')}
          label="禁用解析消息"
          description="禁用警报状态返回false时发送的解析消息[OK]"
        />
      </Field>
      <Field>
        <Checkbox {...register('sendReminder')} label="发送提醒" description="为触发的警报发送其他通知" />
      </Field>
      {currentFormValues.sendReminder && (
        <>
          <Field
            label="间隔发送提醒"
            description="指定提醒的发送频率，例如每30秒、1米、10米、30米或1小时等。在评估规则后发送提醒。提醒的发送频率永远不会超过已配置的警报规则评估间隔。"
          >
            <Input {...register('frequency')} width={8} />
          </Field>
        </>
      )}
    </CollapsableSection>
  );
};
