/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 12:00:59
 * @FilePath: /grafana/public/app/features/alerting/components/BasicSettings.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FC } from 'react';
import { SelectableValue } from '@grafana/data';
import { Field, Input, InputControl, Select } from '@grafana/ui';
import { NotificationChannelOptions } from './NotificationChannelOptions';
import { NotificationSettingsProps } from './NotificationChannelForm';
import { NotificationChannelSecureFields, NotificationChannelType } from '../../../types';

interface Props extends NotificationSettingsProps {
  selectedChannel: NotificationChannelType;
  channels: Array<SelectableValue<string>>;
  secureFields: NotificationChannelSecureFields;
  resetSecureField: (key: string) => void;
}

export const BasicSettings: FC<Props> = ({
  control,
  currentFormValues,
  errors,
  secureFields,
  selectedChannel,
  channels,
  register,
  resetSecureField,
}) => {
  console.log('channels: ', channels);
  return (
    <>
      <Field label="名称" invalid={!!errors.name} error={errors.name && errors.name.message}>
        <Input placeholder="请输入" {...register('name', { required: '请输入名称' })} />
      </Field>
      <Field label="类型">
        <InputControl
          name="type"
          render={({ field: { ref, ...field } }) => {
            console.log('field', field);
            return <Select {...field} options={channels} />;
          }}
          control={control}
          rules={{ required: true }}
        />
      </Field>
      <NotificationChannelOptions
        selectedChannelOptions={selectedChannel.options.filter((o) => o.required)}
        currentFormValues={currentFormValues}
        secureFields={secureFields}
        onResetSecureField={resetSecureField}
        register={register}
        errors={errors}
        control={control}
      />
    </>
  );
};
