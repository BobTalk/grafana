/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 13:34:44
 * @FilePath: /grafana/public/app/features/alerting/components/ChannelSettings.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FC } from 'react';
import { Alert, CollapsableSection } from '@grafana/ui';
import { NotificationChannelOptions } from './NotificationChannelOptions';
import { NotificationSettingsProps } from './NotificationChannelForm';
import { NotificationChannelSecureFields, NotificationChannelType } from '../../../types';

interface Props extends NotificationSettingsProps {
  selectedChannel: NotificationChannelType;
  secureFields: NotificationChannelSecureFields;
  resetSecureField: (key: string) => void;
}

export const ChannelSettings: FC<Props> = ({
  control,
  currentFormValues,
  errors,
  selectedChannel,
  secureFields,
  register,
  resetSecureField,
}) => {
  return (
    <CollapsableSection label={`${selectedChannel.heading}(可选)`} isOpen={false}>
      {selectedChannel.info !== '' && <Alert severity="info" title={selectedChannel.info ?? ''} />}
      <NotificationChannelOptions
        selectedChannelOptions={selectedChannel.options.filter((o) => !o.required)}
        currentFormValues={currentFormValues}
        register={register}
        errors={errors}
        control={control}
        onResetSecureField={resetSecureField}
        secureFields={secureFields}
      />
    </CollapsableSection>
  );
};
