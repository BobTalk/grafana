/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:28
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 13:52:21
 * @FilePath: /grafana/public/app/features/teams/CreateTeam.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { PureComponent } from 'react';
import Page from 'app/core/components/Page/Page';
import { hot } from 'react-hot-loader';
import { Button, Form, Field, Input, FieldSet, Label, Tooltip, Icon } from '@grafana/ui';
import { NavModel } from '@grafana/data';
import { getBackendSrv, locationService } from '@grafana/runtime';
import { connect } from 'react-redux';
import { getNavModel } from 'app/core/selectors/navModel';
import { StoreState } from 'app/types';

export interface Props {
  navModel: NavModel;
}

interface TeamDTO {
  name: string;
  email: string;
}

export class CreateTeam extends PureComponent<Props> {
  create = async (formModel: TeamDTO) => {
    const result = await getBackendSrv().post('/api/teams', formModel);
    if (result.teamId) {
      locationService.push(`/org/teams/edit/${result.teamId}`);
    }
  };
  render() {
    const { navModel } = this.props;

    return (
      <Page navModel={navModel}>
        <Page.Contents>
          <Form onSubmit={this.create}>
            {({ register }) => (
              <FieldSet label="新团队">
                <Field label="名称">
                  <Input {...register('name', { required: true })} width={60} />
                </Field>
                <Field
                  label={
                    <Label>
                      <span>邮箱</span>
                      <Tooltip content="这是可选的，主要用于允许自定义团队头像。">
                        <Icon name="info-circle" style={{ marginLeft: 6 }} />
                      </Tooltip>
                    </Label>
                  }
                >
                  <Input {...register('email')} type="email" placeholder="email@test.com" width={60} />
                </Field>
                <div className="gf-form-button-row">
                  <Button type="submit" variant="primary">
                    创建
                  </Button>
                </div>
              </FieldSet>
            )}
          </Form>
        </Page.Contents>
      </Page>
    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    navModel: getNavModel(state.navIndex, 'teams'),
  };
}

export default hot(module)(connect(mapStateToProps)(CreateTeam));
