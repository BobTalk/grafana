/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 14:49:39
 * @FilePath: /grafana/public/app/features/live/LiveConnectionWarning.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { css } from '@emotion/css';
import { GrafanaTheme2 } from '@grafana/data';
import { config, getGrafanaLiveSrv } from '@grafana/runtime';
import { Alert, stylesFactory } from '@grafana/ui';
import React, { PureComponent } from 'react';
import { Unsubscribable } from 'rxjs';
import { contextSrv } from 'app/core/services/context_srv';

export interface Props {}

export interface State {
  show?: boolean;
}

export class LiveConnectionWarning extends PureComponent<Props, State> {
  subscription?: Unsubscribable;
  styles = getStyle(config.theme2);
  state: State = {};

  componentDidMount() {
    // Only show the error in development mode
    if (process.env.NODE_ENV === 'development') {
      // Wait a second to listen for server errors
      setTimeout(this.initListener, 1500);
    }
  }

  initListener = () => {
    const live = getGrafanaLiveSrv();
    if (live) {
      this.subscription = live.getConnectionState().subscribe({
        next: (v) => {
          this.setState({ show: !v });
        },
      });
    }
  };

  componentWillUnmount() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  render() {
    const { show } = this.state;
    if (show) {
      if (!contextSrv.isSignedIn || !config.liveEnabled) {
        return null; // do not show the warning for anonymous users (and /login page etc)
      }

      return (
        <div className={this.styles.foot}>
          <Alert severity={'warning'} className={this.styles.warn} title="与服务器的连接丢失..." />
        </div>
      );
    }
    return null;
  }
}

const getStyle = stylesFactory((theme: GrafanaTheme2) => {
  return {
    foot: css`
      position: absolute;
      bottom: 0px;
      left: 0px;
      right: 0px;
      z-index: 10000;
      cursor: wait;
      margin: 16px;
    `,
    warn: css`
      border: 2px solid ${theme.colors.warning.main};
      max-width: 400px;
      margin: auto;
      height: 3em;
    `,
  };
});
