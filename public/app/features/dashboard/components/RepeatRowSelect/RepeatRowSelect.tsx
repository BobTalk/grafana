/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 17:04:27
 * @FilePath: /grafana/public/app/features/dashboard/components/RepeatRowSelect/RepeatRowSelect.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FC, useCallback, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { Select } from '@grafana/ui';
import { SelectableValue } from '@grafana/data';

import { getVariables } from '../../../variables/state/selectors';
import { StoreState } from '../../../../types';

export interface Props {
  repeat: string | undefined | null;
  onChange: (name: string | null | undefined) => void;
}

export const RepeatRowSelect: FC<Props> = ({ repeat, onChange }) => {
  const variables = useSelector((state: StoreState) => getVariables(state));

  const variableOptions = useMemo(() => {
    const options = variables.map((item: any) => {
      return { label: item.name, value: item.name };
    });

    if (options.length === 0) {
      options.unshift({
        label: '找不到变量模板',
        value: null,
      });
    }

    options.unshift({
      label: '禁用重复',
      value: null,
    });

    return options;
  }, [variables]);

  const onSelectChange = useCallback((option: SelectableValue<string | null>) => onChange(option.value), [onChange]);

  return <Select value={repeat} onChange={onSelectChange} options={variableOptions} />;
};
