import React from 'react';
import { css } from '@emotion/css';
import { NavModel } from '@grafana/data';
import Page from '../../core/components/Page/Page';
import { LicenseChrome } from './LicenseChrome';
import { LinkButton } from '@grafana/ui';
import { hot } from 'react-hot-loader';
import { StoreState } from '../../types';
import { getNavModel } from '../../core/selectors/navModel';
import { connect } from 'react-redux';

interface Props {
  navModel: NavModel;
}

export const UpgradePage: React.FC<Props> = ({ navModel }) => {
  return (
    <Page navModel={navModel}>
      <Page.Contents>
        <UpgradeInfo
          editionNotice="您正在运行Grafana的开源版本。
          您必须安装企业版才能启用企业版功能。"
        />
      </Page.Contents>
    </Page>
  );
};

const titleStyles = { fontWeight: 500, fontSize: '26px', lineHeight: '123%' };

interface UpgradeInfoProps {
  editionNotice?: string;
}

export const UpgradeInfo: React.FC<UpgradeInfoProps> = ({ editionNotice }) => {
  const columnStyles = css`
    display: grid;
    grid-template-columns: 100%;
    column-gap: 20px;
    row-gap: 40px;

    @media (min-width: 1050px) {
      grid-template-columns: 50% 50%;
    }
  `;

  return (
    <LicenseChrome header="Grafana企业" subheader="获得免费试用" editionNotice={editionNotice}>
      <div className={columnStyles}>
        <FeatureInfo />
        <ServiceInfo />
      </div>
    </LicenseChrome>
  );
};

const GetEnterprise: React.FC = () => {
  return (
    <div style={{ marginTop: '40px', marginBottom: '30px' }}>
      <h2 style={titleStyles}>获取Grafana Enterprise</h2>
      <CallToAction />
      <p style={{ paddingTop: '12px' }}>您可以免费使用试用版30天。我们将在试用期结束前五天提醒您。</p>
    </div>
  );
};

const CallToAction: React.FC = () => {
  return (
    <LinkButton
      variant="primary"
      size="lg"
      href="https://grafana.com/contact?about=grafana-enterprise&utm_source=grafana-upgrade-page"
    >
      联系我们并获得免费试用
    </LinkButton>
  );
};

const ServiceInfo: React.FC = () => {
  return (
    <div>
      <h4>服务</h4>

      <List>
        <Item title="企业插件" image="public/img/licensing/plugin_enterprise.svg" />
        <Item title="关键SLA：2小时" image="public/img/licensing/sla.svg" />
        <Item title="无限专家支持" image="public/img/licensing/customer_support.svg">
          24 x 7 x 365 support via
          <List nested={true}>
            <Item title="邮箱" />
            <Item title="专用Slack频道" />
            <Item title="手机" />
          </List>
        </Item>
        <Item title="联手服务" image="public/img/licensing/handinhand_support.svg">
          在升级过程中
        </Item>
      </List>

      <div style={{ marginTop: '20px' }}>
        <strong>还包含:</strong>
        <br />
        赔偿，与Grafana实验室就未来的优先事项进行合作，并由Grafana核心团队进行培训。
      </div>

      <GetEnterprise />
    </div>
  );
};

const FeatureInfo: React.FC = () => {
  return (
    <div style={{ paddingRight: '11px' }}>
      <h4>高级功能</h4>
      <FeatureListing />
    </div>
  );
};

const FeatureListing: React.FC = () => {
  return (
    <List>
      <Item title="数据源权限" />
      <Item title="报告" />
      <Item title="SAML身份验证" />
      <Item title="增强LDAP集成" />
      <Item title="同步团队">LDAP, GitHub OAuth, Auth Proxy, Okta</Item>
      <Item title="白色标签" />
      <Item title="审核" />
      <Item title="更新运行设置" />
      <Item title="Grafana用法见解">
        <List nested={true}>
          <Item title="按搜索中的受欢迎程度对仪表板进行排序" />
          <Item title="查找未使用的仪表板" />
          <Item title="仪表板使用情况统计抽屉" />
          <Item title="仪表板状态指示灯" />
        </List>
      </Item>
      <Item title="企业插件">
        <List nested={true}>
          <Item title="Oracle" />
          <Item title="Splunk" />
          <Item title="Service Now" />
          <Item title="Dynatrace" />
          <Item title="New Relic" />
          <Item title="DataDog" />
          <Item title="AppDynamics" />
          <Item title="Amazon Timestream" />
          <Item title="SAP HANA®" />
        </List>
      </Item>
    </List>
  );
};

interface ListProps {
  nested?: boolean;
}

const List: React.FC<ListProps> = ({ children, nested }) => {
  const listStyle = css`
    display: flex;
    flex-direction: column;
    padding-top: 8px;

    > div {
      margin-bottom: ${nested ? 0 : 8}px;
    }
  `;

  return <div className={listStyle}>{children}</div>;
};

interface ItemProps {
  title: string;
  image?: string;
}

const Item: React.FC<ItemProps> = ({ children, title, image }) => {
  const imageUrl = image ? image : 'public/img/licensing/checkmark.svg';
  const itemStyle = css`
    display: flex;

    > img {
      display: block;
      height: 22px;
      flex-grow: 0;
      padding-right: 12px;
    }
  `;
  const titleStyle = css`
    font-weight: 500;
    line-height: 1.7;
  `;

  return (
    <div className={itemStyle}>
      <img src={imageUrl} />
      <div>
        <div className={titleStyle}>{title}</div>
        {children}
      </div>
    </div>
  );
};

const mapStateToProps = (state: StoreState) => ({
  navModel: getNavModel(state.navIndex, 'upgrading'),
});

export default hot(module)(connect(mapStateToProps)(UpgradePage));
