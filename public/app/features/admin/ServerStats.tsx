/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 14:01:36
 * @FilePath: /grafana/public/app/features/admin/ServerStats.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { PureComponent } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import { Icon, Tooltip } from '@grafana/ui';
import { NavModel } from '@grafana/data';
import { StoreState } from 'app/types';
import { getNavModel } from 'app/core/selectors/navModel';
import Page from 'app/core/components/Page/Page';
import { getServerStats, ServerStat } from './state/apis';

interface Props {
  navModel: NavModel;
  getServerStats: () => Promise<ServerStat[]>;
}

interface State {
  stats: ServerStat[];
  isLoading: boolean;
}

export class ServerStats extends PureComponent<Props, State> {
  state: State = {
    stats: [],
    isLoading: true,
  };

  async componentDidMount() {
    try {
      const stats = await this.props.getServerStats();
      this.setState({ stats, isLoading: false });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { navModel } = this.props;
    const { stats, isLoading } = this.state;

    return (
      <Page navModel={navModel}>
        <Page.Contents isLoading={isLoading}>
          <table className="filter-table form-inline">
            <thead>
              <tr>
                <th>名称</th>
                <th>数值</th>
              </tr>
            </thead>
            <tbody>{stats.map(StatItem)}</tbody>
          </table>
        </Page.Contents>
      </Page>
    );
  }
}

function StatItem(stat: ServerStat) {
  return (
    <tr key={stat.name}>
      <td>
        {stat.name}{' '}
        {stat.tooltip && (
          <Tooltip content={stat.tooltip} placement={'top'}>
            <Icon name={'info-circle'} />
          </Tooltip>
        )}
      </td>
      <td>{stat.value}</td>
    </tr>
  );
}

const mapStateToProps = (state: StoreState) => ({
  navModel: getNavModel(state.navIndex, 'server-stats'),
  getServerStats: getServerStats,
});

export default hot(module)(connect(mapStateToProps)(ServerStats));
