/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:28
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 14:37:56
 * @FilePath: /grafana/public/app/features/profile/UserProfileEditForm.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FC } from 'react';
import { Button, Field, FieldSet, Form, Icon, Input, Tooltip } from '@grafana/ui';
import { UserDTO } from 'app/types';
import config from 'app/core/config';
import { ProfileUpdateFields } from './types';

export interface Props {
  user: UserDTO | null;
  isSavingUser: boolean;
  updateProfile: (payload: ProfileUpdateFields) => void;
}

const { disableLoginForm } = config;

export const UserProfileEditForm: FC<Props> = ({ user, isSavingUser, updateProfile }) => {
  const onSubmitProfileUpdate = (data: ProfileUpdateFields) => {
    updateProfile(data);
  };

  return (
    <Form onSubmit={onSubmitProfileUpdate} validateOn="onBlur">
      {({ register, errors }) => {
        return (
          <FieldSet label="编辑配置文件">
            <Field label="名称" invalid={!!errors.name} error="请输入名称" disabled={disableLoginForm}>
              <Input
                {...register('name', { required: true })}
                id="edit-user-profile-name"
                placeholder="请输入"
                defaultValue={user?.name ?? ''}
                suffix={<InputSuffix />}
              />
            </Field>
            <Field label="邮箱" invalid={!!errors.email} error="请输入邮箱地址" disabled={disableLoginForm}>
              <Input
                {...register('email', { required: true })}
                id="edit-user-profile-email"
                placeholder="请输入"
                defaultValue={user?.email ?? ''}
                suffix={<InputSuffix />}
              />
            </Field>
            <Field label="用户名" disabled={disableLoginForm}>
              <Input
                {...register('login')}
                id="edit-user-profile-username"
                defaultValue={user?.login ?? ''}
                placeholder="请输入"
                suffix={<InputSuffix />}
              />
            </Field>
            <div className="gf-form-button-row">
              <Button variant="primary" disabled={isSavingUser} aria-label="Edit user profile save button">
                保存
              </Button>
            </div>
          </FieldSet>
        );
      }}
    </Form>
  );
};

export default UserProfileEditForm;

const InputSuffix: FC = () => {
  return disableLoginForm ? (
    <Tooltip content="登录详细信息已锁定，因为它们在另一个系统中进行管理。">
      <Icon name="lock" />
    </Tooltip>
  ) : null;
};
