/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:28
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 14:32:48
 * @FilePath: /grafana/public/app/features/profile/ChangePasswordPage.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React from 'react';
import { useMount } from 'react-use';
import { hot } from 'react-hot-loader';
import { connect, ConnectedProps } from 'react-redux';
import { NavModel } from '@grafana/data';

import { StoreState } from 'app/types';
import { getNavModel } from 'app/core/selectors/navModel';
import Page from 'app/core/components/Page/Page';
import { ChangePasswordForm } from './ChangePasswordForm';
import { changePassword, loadUser } from './state/actions';

export interface OwnProps {
  navModel: NavModel;
}

function mapStateToProps(state: StoreState) {
  const userState = state.user;
  const { isUpdating, user } = userState;
  return {
    navModel: getNavModel(state.navIndex, `change-password`),
    isUpdating,
    user,
  };
}

const mapDispatchToProps = {
  loadUser,
  changePassword,
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type Props = OwnProps & ConnectedProps<typeof connector>;

export function ChangePasswordPage({ navModel, loadUser, isUpdating, user, changePassword }: Props) {
  useMount(() => loadUser());

  return (
    <Page navModel={navModel}>
      <Page.Contents isLoading={!Boolean(user)}>
        {user ? (
          <>
            <h3 className="page-heading">密码修改</h3>
            <ChangePasswordForm user={user} onChangePassword={changePassword} isSaving={isUpdating} />
          </>
        ) : null}
      </Page.Contents>
    </Page>
  );
}

export default hot(module)(connector(ChangePasswordPage));
