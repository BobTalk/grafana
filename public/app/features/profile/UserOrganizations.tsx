/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:28
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 14:39:25
 * @FilePath: /grafana/public/app/features/profile/UserOrganizations.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { PureComponent } from 'react';
import { UserDTO, UserOrg } from 'app/types';
import { Button, LoadingPlaceholder } from '@grafana/ui';

export interface Props {
  user: UserDTO | null;
  orgs: UserOrg[];
  isLoading: boolean;
  setUserOrg: (org: UserOrg) => void;
}

export class UserOrganizations extends PureComponent<Props> {
  render() {
    const { isLoading, orgs, user } = this.props;

    if (isLoading) {
      return <LoadingPlaceholder text="Loading organizations..." />;
    }

    if (orgs.length === 0) {
      return null;
    }

    return (
      <div>
        <h3 className="page-sub-heading">组织</h3>
        <div className="gf-form-group">
          <table className="filter-table form-inline" aria-label="User organizations table">
            <thead>
              <tr>
                <th>名称</th>
                <th>角色</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {orgs.map((org: UserOrg, index) => {
                return (
                  <tr key={index}>
                    <td>{org.name}</td>
                    <td>{org.role}</td>
                    <td className="text-right">
                      {org.orgId === user?.orgId ? (
                        <Button variant="secondary" size="sm" disabled>
                          Current
                        </Button>
                      ) : (
                        <Button
                          variant="secondary"
                          size="sm"
                          onClick={() => {
                            this.props.setUserOrg(org);
                          }}
                        >
                          选择
                        </Button>
                      )}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default UserOrganizations;
