/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:28
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 14:50:43
 * @FilePath: /grafana/public/app/features/profile/ChangePasswordForm.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FC } from 'react';
import { css } from '@emotion/css';
import { Button, Field, Form, HorizontalGroup, Input, LinkButton } from '@grafana/ui';

import config from 'app/core/config';
import { UserDTO } from 'app/types';
import { ChangePasswordFields } from './types';

export interface Props {
  user: UserDTO;
  isSaving: boolean;
  onChangePassword: (payload: ChangePasswordFields) => void;
}

export const ChangePasswordForm: FC<Props> = ({ user, onChangePassword, isSaving }) => {
  const { ldapEnabled, authProxyEnabled, disableLoginForm } = config;
  const authSource = user.authLabels?.length && user.authLabels[0];

  if (ldapEnabled || authProxyEnabled) {
    return <p>You cannot change password when LDAP or auth proxy authentication is enabled.</p>;
  }
  if (authSource && disableLoginForm) {
    return <p>Password cannot be changed here.</p>;
  }

  return (
    <div
      className={css`
        max-width: 400px;
      `}
    >
      <Form onSubmit={onChangePassword}>
        {({ register, errors, getValues }) => {
          return (
            <>
              <Field label="旧密码" invalid={!!errors.oldPassword} error={errors?.oldPassword?.message}>
                <Input
                  type="password"
                  placeholder="请输入"
                  {...register('oldPassword', { required: '请输入旧密码' })}
                />
              </Field>

              <Field label="新密码" invalid={!!errors.newPassword} error={errors?.newPassword?.message}>
                <Input
                  type="password"
                  placeholder="请输入"
                  {...register('newPassword', {
                    required: '请输入新密码',
                    validate: {
                      confirm: (v) => v === getValues().confirmNew || '密码必须匹配',
                      old: (v) => v !== getValues().oldPassword || `新密码不能与旧密码相同`,
                    },
                  })}
                />
              </Field>

              <Field label="确认密码" invalid={!!errors.confirmNew} error={errors?.confirmNew?.message}>
                <Input
                  type="password"
                  placeholder="请输入"
                  {...register('confirmNew', {
                    required: '请再次输入新密码',
                    validate: (v) => v === getValues().newPassword || '密码必须匹配',
                  })}
                />
              </Field>
              <HorizontalGroup>
                <Button variant="primary" disabled={isSaving}>
                  确认
                </Button>
                <LinkButton variant="secondary" href={`${config.appSubUrl}/profile`} fill="outline">
                  取消
                </LinkButton>
              </HorizontalGroup>
            </>
          );
        }}
      </Form>
    </div>
  );
};
