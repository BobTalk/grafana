/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 11:23:32
 * @FilePath: /grafana/public/app/core/components/Select/SortPicker.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FC } from 'react';
import { useAsync } from 'react-use';
import { Icon, IconName, Select } from '@grafana/ui';
import { SelectableValue } from '@grafana/data';
import { DEFAULT_SORT } from 'app/features/search/constants';
import { SearchSrv } from '../../services/search_srv';

const searchSrv = new SearchSrv();

export interface Props {
  onChange: (sortValue: SelectableValue) => void;
  value?: string;
  placeholder?: string;
  filter?: string[];
}

const getSortOptions = (filter?: string[]) => {
  return searchSrv.getSortOptions().then(({ sortOptions }) => {
    const filteredOptions = filter ? sortOptions.filter((o: any) => filter.includes(o.name)) : sortOptions;
    return filteredOptions.map((opt: any) => ({ label: opt.displayName, value: opt.name }));
  });
};

export const SortPicker: FC<Props> = ({ onChange, value, placeholder, filter }) => {
  // Using sync Select and manual options fetching here since we need to find the selected option by value
  const { loading, value: options } = useAsync<SelectableValue[]>(() => getSortOptions(filter), []);

  const selected = options?.find((opt) => opt.value === value);
  return !loading ? (
    <Select
      key={value}
      width={25}
      onChange={onChange}
      value={selected ?? null}
      options={options}
      placeholder={placeholder ?? `排序 (默认 ${DEFAULT_SORT.label})`}
      prefix={<Icon name={(value?.includes('asc') ? 'sort-amount-up' : 'sort-amount-down') as IconName} />}
    />
  ) : null;
};
