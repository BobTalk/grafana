/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 20:54:15
 * @FilePath: /grafana/public/app/core/components/Select/DashboardPicker.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FC } from 'react';
import debounce from 'debounce-promise';
import { SelectableValue } from '@grafana/data';
import { AsyncSelect } from '@grafana/ui';
import { backendSrv } from 'app/core/services/backend_srv';
import { DashboardSearchHit } from 'app/features/search/types';

export interface DashboardPickerItem extends Pick<DashboardSearchHit, 'uid' | 'id'> {
  value: number;
  label: string;
}

export interface Props {
  onChange: (dashboard: DashboardPickerItem) => void;
  value?: SelectableValue;
  width?: number;
  isClearable?: boolean;
  invalid?: boolean;
  disabled?: boolean;
}

const getDashboards = (query = '') => {
  return backendSrv.search({ type: 'dash-db', query }).then((result: DashboardSearchHit[]) => {
    return result.map((item: DashboardSearchHit) => ({
      id: item.id,
      uid: item.uid,
      value: item.id,
      label: `${item?.folderTitle ?? 'General'}/${item.title}`,
    }));
  });
};

export const DashboardPicker: FC<Props> = ({ onChange, value, width, isClearable = false, invalid, disabled }) => {
  const debouncedSearch = debounce(getDashboards, 300);

  return (
    <AsyncSelect
      width={width}
      isClearable={isClearable}
      defaultOptions={true}
      loadOptions={debouncedSearch}
      onChange={onChange}
      placeholder="请选择"
      noOptionsMessage="暂无仪表板信息"
      value={value}
      invalid={invalid}
      disabled={disabled}
    />
  );
};
