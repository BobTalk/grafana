/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 16:44:23
 * @FilePath: /grafana/public/app/core/components/PanelTypeFilter/PanelTypeFilter.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { useCallback, useMemo, useState } from 'react';
import { GrafanaTheme2, PanelPluginMeta, SelectableValue } from '@grafana/data';
import { getAllPanelPluginMeta } from '../../../features/dashboard/components/VizTypePicker/VizTypePicker';
import { Icon, resetSelectStyles, Select, useStyles2 } from '@grafana/ui';
import { css } from '@emotion/css';

export interface Props {
  onChange: (plugins: PanelPluginMeta[]) => void;
  maxMenuHeight?: number;
}

export const PanelTypeFilter = ({ onChange: propsOnChange, maxMenuHeight }: Props): JSX.Element => {
  const plugins = useMemo<PanelPluginMeta[]>(() => {
    return getAllPanelPluginMeta();
  }, []);
  const options = useMemo(
    () =>
      plugins
        .map((p) => ({ label: p.name, imgUrl: p.info.logos.small, value: p }))
        .sort((a, b) => a.label?.localeCompare(b.label)),
    [plugins]
  );
  const [value, setValue] = useState<Array<SelectableValue<PanelPluginMeta>>>([]);
  const onChange = useCallback(
    (plugins: Array<SelectableValue<PanelPluginMeta>>) => {
      const changedPlugins = [];
      for (const plugin of plugins) {
        if (plugin.value) {
          changedPlugins.push(plugin.value);
        }
      }
      propsOnChange(changedPlugins);
      setValue(plugins);
    },
    [propsOnChange]
  );
  const styles = useStyles2(getStyles);

  const selectOptions = {
    defaultOptions: true,
    getOptionLabel: (i: any) => i.label,
    getOptionValue: (i: any) => i.value,
    isMulti: true,
    noOptionsMessage: '找不到面板类型',
    placeholder: '请选择',
    styles: resetSelectStyles(),
    maxMenuHeight,
    options,
    value,
    onChange,
  };

  return (
    <div className={styles.container}>
      {value.length > 0 && (
        <span className={styles.clear} onClick={() => onChange([])}>
          清除类型
        </span>
      )}
      <Select {...selectOptions} prefix={<Icon name="filter" />} aria-label="Panel Type filter" />
    </div>
  );
};

function getStyles(theme: GrafanaTheme2) {
  return {
    container: css`
      label: container;
      position: relative;
      min-width: 180px;
      flex-grow: 1;
    `,
    clear: css`
      label: clear;
      text-decoration: underline;
      font-size: ${theme.spacing(1.5)};
      position: absolute;
      top: -${theme.spacing(2.75)};
      right: 0;
      cursor: pointer;
      color: ${theme.colors.text.link};

      &:hover {
        color: ${theme.colors.text.maxContrast};
      }
    `,
  };
}
