/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 14:26:10
 * @FilePath: /grafana/public/app/core/components/OrgSwitcher.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React from 'react';

import { getBackendSrv } from '@grafana/runtime';
import { UserOrgDTO } from '@grafana/data';
import { Modal, Button, CustomScrollbar } from '@grafana/ui';

import { contextSrv } from 'app/core/services/context_srv';
import config from 'app/core/config';
import { css } from '@emotion/css';

interface Props {
  onDismiss: () => void;
}

interface State {
  orgs: UserOrgDTO[];
}

export class OrgSwitcher extends React.PureComponent<Props, State> {
  state: State = {
    orgs: [],
  };

  componentDidMount() {
    this.getUserOrgs();
  }

  getUserOrgs = async () => {
    const orgs: UserOrgDTO[] = await getBackendSrv().get('/api/user/orgs');
    this.setState({ orgs });
  };

  setCurrentOrg = async (org: UserOrgDTO) => {
    await getBackendSrv().post(`/api/user/using/${org.orgId}`);
    this.setWindowLocation(`${config.appSubUrl}${config.appSubUrl.endsWith('/') ? '' : '/'}?orgId=${org.orgId}`);
  };

  setWindowLocation(href: string) {
    window.location.href = href;
  }

  render() {
    const { onDismiss } = this.props;
    const { orgs } = this.state;

    const currentOrgId = contextSrv.user.orgId;
    const contentClassName = css({
      display: 'flex',
      maxHeight: 'calc(85vh - 42px)',
    });

    return (
      <Modal
        title="切换组织"
        icon="arrow-random"
        onDismiss={onDismiss}
        isOpen={true}
        contentClassName={contentClassName}
      >
        <CustomScrollbar autoHeightMin="100%">
          <table className="filter-table form-inline">
            <thead>
              <tr>
                <th>名称</th>
                <th>角色</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {orgs.map((org) => (
                <tr key={org.orgId}>
                  <td>{org.name}</td>
                  <td>{org.role}</td>
                  <td className="text-right">
                    {org.orgId === currentOrgId ? (
                      <Button size="sm">当前</Button>
                    ) : (
                      <Button variant="secondary" size="sm" onClick={() => this.setCurrentOrg(org)}>
                        切换
                      </Button>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </CustomScrollbar>
      </Modal>
    );
  }
}
