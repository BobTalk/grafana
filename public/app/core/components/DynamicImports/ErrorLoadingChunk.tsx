/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 16:06:11
 * @FilePath: /grafana/public/app/core/components/DynamicImports/ErrorLoadingChunk.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FunctionComponent } from 'react';
import { Button, stylesFactory } from '@grafana/ui';
import { css } from '@emotion/css';
import { useUrlParams } from 'app/core/navigation/hooks';

const getStyles = stylesFactory(() => {
  return css`
    width: 508px;
    margin: 128px auto;
  `;
});

interface Props {
  error: Error | null;
}

export const ErrorLoadingChunk: FunctionComponent<Props> = ({ error }) => {
  const [params, updateUrlParams] = useUrlParams();

  if (!params.get('chunkNotFound')) {
    updateUrlParams({ chunkNotFound: true }, true);
    window.location.reload();
  }

  return (
    <div className={getStyles()}>
      <h2>Unable to find application file</h2>
      <br />
      <h2 className="page-heading">Grafana has likely been updated. Please try reloading the page.</h2>
      <br />
      <div className="gf-form-group">
        <Button size="md" variant="secondary" icon="repeat" onClick={() => window.location.reload()}>
          Reload
        </Button>
      </div>
      <details style={{ whiteSpace: 'pre-wrap' }}>
        {error && error.message ? error.message : '出现意外错误'}
        <br />
        {error && error.stack ? error.stack : null}
      </details>
    </div>
  );
};

ErrorLoadingChunk.displayName = 'ErrorLoadingChunk';
