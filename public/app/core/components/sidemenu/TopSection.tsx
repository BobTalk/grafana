/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 22:43:59
 * @FilePath: /grafana/public/app/core/components/sidemenu/TopSection.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FC } from 'react';
import { cloneDeep, filter } from 'lodash';
import TopSectionItem from './TopSectionItem';
import config from '../../config';
import { locationService } from '@grafana/runtime';

const TopSection: FC<any> = () => {
  const navTree = cloneDeep(config.bootData.navTree);
  console.log('config: ', config.bootData);
  console.log('navTree: ', navTree);
  const mainLinks = filter(navTree, (item) => !item.hideFromMenu);
  const searchLink = {
    text: '搜索',
    icon: 'search',
  };

  const onOpenSearch = () => {
    locationService.partial({ search: 'open' });
  };

  return (
    <div className="sidemenu__top">
      <TopSectionItem link={searchLink} onClick={onOpenSearch} />
      {mainLinks.map((link, index) => {
        return <TopSectionItem link={link} key={`${link.id}-${index}`} />;
      })}
    </div>
  );
};

export default TopSection;
