package alerting

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/grafana/grafana/pkg/bus"
	"github.com/grafana/grafana/pkg/models"
	"github.com/grafana/grafana/pkg/tests/testinfra"
)

func TestAvailableChannels(t *testing.T) {
	dir, path := testinfra.CreateGrafDir(t, testinfra.GrafanaOpts{
		EnableFeatureToggles: []string{"ngalert"},
		DisableAnonymous:     true,
	})

	store := testinfra.SetUpDatabase(t, dir)
	store.Bus = bus.GetBus()
	grafanaListedAddr := testinfra.StartGrafana(t, dir, path, store)

	// Create a user to make authenticated requests
	require.NoError(t, createUser(t, store, models.ROLE_EDITOR, "grafana", "password"))

	alertsURL := fmt.Sprintf("http://grafana:password@%s/api/alert-notifiers", grafanaListedAddr)
	// nolint:gosec
	resp, err := http.Get(alertsURL)
	require.NoError(t, err)
	t.Cleanup(func() {
		err := resp.Body.Close()
		require.NoError(t, err)
	})
	b, err := ioutil.ReadAll(resp.Body)
	require.NoError(t, err)
	require.Equal(t, 200, resp.StatusCode)
	require.JSONEq(t, expAvailableChannelJsonOutput, string(b))
}

var expAvailableChannelJsonOutput = `
[
  {
    "type": "dingding",
    "name": "DingDing",
    "heading": "DingDing settings",
    "description": "Sends HTTP POST request to DingDing",
    "info": "",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label": "Url",
        "description": "",
        "placeholder": "https://oapi.dingtalk.com/robot/send?access_token=xxxxxxxxx",
        "propertyName": "url",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "select",
        "inputType": "",
        "label": "Message Type",
        "description": "",
        "placeholder": "",
        "propertyName": "msgType",
        "selectOptions": [
          {
            "value": "link",
            "label": "Link"
          },
          {
            "value": "actionCard",
            "label": "ActionCard"
          }
        ],
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "textarea",
        "inputType": "",
        "label": "Message",
        "description": "",
        "placeholder": "{{ template \"default.message\" . }}",
        "propertyName": "message",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      }
    ]
  },
  {
    "type": "kafka",
    "name": "Kafka REST代理",
    "heading": "Kafka settings",
    "description": "向Kafka Rest代理发送通知",
    "info": "",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label":  "Kafka REST代理",
        "description": "",
        "placeholder": "http://localhost:8082",
        "propertyName": "kafkaRestProxy",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label":  "话题",
        "description": "",
        "placeholder":  "话题",
        "propertyName": "kafkaTopic",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": false
      }
    ]
  },
  {
    "type": "email",
    "name": "邮件",
    "heading": "邮件设置",
    "description": "使用Grafana服务器配置的SMTP设置发送通知",
    "info": "",
    "options": [
      {
        "element": "checkbox",
        "inputType": "",
        "label":  "单邮件",
        "description": "向所有收件人发送一封电子邮件",
        "placeholder": "",
        "propertyName": "singleEmail",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "textarea",
        "inputType": "",
        "label": "地址",
        "description":"您可以使用“；”分隔符输入多个电子邮件地址",
        "placeholder": "",
        "propertyName": "addresses",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "textarea",
        "inputType": "",
        "label": "信息",
        "description": "要包含在电子邮件中的可选消息,可以使用模板变量",
        "placeholder": "",
        "propertyName": "message",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      }
    ]
  },
  {
    "type": "pagerduty",
    "name": "PagerDuty",
    "heading": "PagerDuty settings",
    "description": "向PagerDuty发送通知",
    "info": "",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label": "Integration密钥",
        "description": "",
        "placeholder": "Pagerduty集成密钥",
        "propertyName": "integrationKey",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": true
      },
      {
        "element": "select",
        "inputType": "",
        "label": "程度",
        "description": "",
        "placeholder": "",
        "propertyName": "severity",
        "selectOptions": [
          {
            "value": "critical",
            "label": "严重",
          },
          {
            "value": "error",
            "label": "错误",
          },
          {
            "value": "warning",
            "label": "警告",
          },
          {
            "value": "info",
            "label": "信息",
          }
        ],
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "类/类型",
        "description": "事件的类/类型，例如“ping failure”或“cpu load”",
        "placeholder": "",
        "propertyName": "class",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "Component",
        "description": "负责事件的源机器的组件，例如mysql或eth0",
        "placeholder": "Grafana",
        "propertyName": "component",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "Group",
        "description": "服务组件的逻辑分组，例如“应用程序堆栈”",
        "placeholder": "",
        "propertyName": "group",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "textarea",
        "inputType": "",
        "label": "Summary",
        "description": "您可以使用模板进行摘要",
        "placeholder": "{{ template \"default.message\" . }}",
        "propertyName": "summary",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      }
    ]
  },
  {
    "type": "victorops",
    "name": "VictorOps",
    "heading": "VictorOps settings",
    "description": "向VictorOps发送通知",
    "info": "",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label":  "地址",
        "description": "",
        "placeholder": "VictorOps地址",
        "propertyName": "url",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "select",
        "inputType": "",
        "label": "消息类型",
        "description": "",
        "placeholder": "",
        "propertyName": "messageType",
        "selectOptions": [
          {
            "value": "CRITICAL",
            "label": "严重"
          },
          {
            "value": "WARNING",
            "label": "警告"
          }
        ],
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      }
    ]
  },
  {
    "type": "pushover",
    "name": "Pushover",
    "description": "向Pushover API发送HTTP POST请求",
    "heading": "Pushover settings",
    "info": "",
    "options": [
        {
            "element": "input",
            "inputType": "text",
            "label": "API令牌",
            "description": "",
            "placeholder": "Application token",
            "propertyName": "apiToken",
            "selectOptions": null,
            "showWhen": {
                "field": "",
                "is": ""
            },
            "required": true,
            "validationRule": "",
            "secure": true
        },
        {
            "element": "input",
            "inputType": "text",
            "label": "用户密钥",
            "description": "",
            "placeholder": "逗号分隔列表",
            "propertyName": "userKey",
            "selectOptions": null,
            "showWhen": {
                "field": "",
                "is": ""
            },
            "required": true,
            "validationRule": "",
            "secure": true
        },
        {
            "element": "input",
            "inputType": "text",
            "label": "设备（可选）",
            "description": "",
            "placeholder": "逗号分隔列表；保留为空以发送到所有设备",
            "propertyName": "device",
            "selectOptions": null,
            "showWhen": {
                "field": "",
                "is": ""
            },
            "required": false,
            "validationRule": "",
            "secure": false
        },
        {
            "element": "select",
            "inputType": "",
            "label":  "警报优先级",
            "description": "",
            "placeholder": "",
            "propertyName": "priority",
            "selectOptions": [
                {
                    "value": "2",
                    "label": "紧急"
                },
                {
                    "value": "1",
                    "label": "高"
                },
                {
                    "value": "0",
                    "label": "普通"
                },
                {
                    "value": "-1",
                    "label": "低"
                },
                {
                    "value": "-2",
                    "label": "最低"
                }
            ],
            "showWhen": {
                "field": "",
                "is": ""
            },
            "required": false,
            "validationRule": "",
            "secure": false
        },
        {
            "element": "select",
            "inputType": "",
            "label": "OK优先级",
            "description": "",
            "placeholder": "",
            "propertyName": "okPriority",
            "selectOptions": [
                {
                    "value": "2",
                    "label": "紧急"
                },
                {
                    "value": "1",
                    "label": "高"
                },
                {
                    "value": "0",
                    "label": "普通"
                },
                {
                    "value": "-1",
                    "label": "低"
                },
                {
                    "value": "-2",
                    "label": "最低"
                }
            ],
            "showWhen": {
                "field": "",
                "is": ""
            },
            "required": false,
            "validationRule": "",
            "secure": false
        },
        {
            "element": "input",
            "inputType": "text",
            "label": "重试（仅用于紧急优先级）",
            "description":"Pushover服务器向用户发送相同警报或OK通知的频率（以秒为单位）。",
            "placeholder": "至少30秒",
            "propertyName": "retry",
            "selectOptions": null,
            "showWhen": {
                "field": "",
                "is": ""
            },
            "required": false,
            "validationRule": "",
            "secure": false
        },
        {
            "element": "input",
            "inputType": "text",
            "label": "过期（仅用于紧急优先级）",
            "description": "将继续重试警报或OK通知的秒数。",
            "placeholder": "最长86400秒",
            "propertyName": "expire",
            "selectOptions": null,
            "showWhen": {
                "field": "",
                "is": ""
            },
            "required": false,
            "validationRule": "",
            "secure": false
        },
        {
            "element": "select",
            "inputType": "",
            "label": "报警声音",
            "description": "",
            "placeholder": "",
            "propertyName": "sound",
            "selectOptions": [
                {
                    "value": "default",
                    "label": "默认"
                },
                {
                    "value": "pushover",
                    "label": "推送"
                },
                {
                    "value": "bike",
                    "label": "自行车"
                },
                {
                    "value": "bugle",
                    "label": "军号"
                },
                {
                    "value": "cashregister",
                    "label": "收银机",
                },
                {
                    "value": "classical",
                    "label": "古典",
                },
                {
                    "value": "cosmic",
                    "label": "宇宙",
                },
                {
                    "value": "falling",
                    "label": "坠落",
                },
                {
                    "value": "gamelan",
                    "label": "Gamelan"
                },
                {
                    "value": "incoming",
                    "label": "接收",
                },
                {
                    "value": "intermission",
                    "label": "间歇",
                },
                {
                    "value": "magic",
                    "label": "魔术",
                },
                {
                    "value": "mechanical",
                    "label": "机械",
                },
                {
                    "value": "pianobar",
                    "label": "钢琴酒吧",
                },
                {
                    "value": "siren",
                    "label": "汽笛",
                },
                {
                    "value": "spacealarm",
                    "label": "空间报警器",
                },
                {
                    "value": "tugboat",
                    "label": "拖船",
                },
                {
                    "value": "alien",
                    "label": "外星人",
                },
                {
                    "value": "climb",
                    "label": "攀登",
                },
                {
                    "value": "persistent",
                    "label": "持久",
                },
                {
                    "value": "echo",
                    "label": "回响",
                },
                {
                    "value": "updown",
                    "label": "七上八下",
                },
                {
                    "value": "none",
                    "label": "无",
                }
            ],
            "showWhen": {
                "field": "",
                "is": ""
            },
            "required": false,
            "validationRule": "",
            "secure": false
        },
        {
            "element": "select",
            "inputType": "",
            "label":"OK声音",
            "description": "",
            "placeholder": "",
            "propertyName": "okSound",
            "selectOptions": [
                {
                    "value": "default",
                    "label": "默认"
                },
                {
                    "value": "pushover",
                    "label": "推送"
                },
                {
                    "value": "bike",
                    "label": "自行车"
                },
                {
                    "value": "bugle",
                    "label": "军号"
                },
                {
                    "value": "cashregister",
                    "label": "收银机",
                },
                {
                    "value": "classical",
                    "label": "古典",
                },
                {
                    "value": "cosmic",
                    "label": "宇宙",
                },
                {
                    "value": "falling",
                    "label": "坠落",
                },
                {
                    "value": "gamelan",
                    "label": "Gamelan"
                },
                {
                    "value": "incoming",
                    "label": "接收",
                },
                {
                    "value": "intermission",
                    "label": "间歇",
                },
                {
                    "value": "magic",
                    "label": "魔术",
                },
                {
                    "value": "mechanical",
                    "label": "机械",
                },
                {
                    "value": "pianobar",
                    "label": "钢琴酒吧",
                },
                {
                    "value": "siren",
                    "label": "汽笛",
                },
                {
                    "value": "spacealarm",
                    "label": "空间报警器",
                },
                {
                    "value": "tugboat",
                    "label": "拖船",
                },
                {
                    "value": "alien",
                    "label": "外星人",
                },
                {
                    "value": "climb",
                    "label": "攀登",
                },
                {
                    "value": "persistent",
                    "label": "持久",
                },
                {
                    "value": "echo",
                    "label": "回响",
                },
                {
                    "value": "updown",
                    "label": "七上八下",
                },
                {
                    "value": "none",
                    "label": "无",
                }
            ],
            "showWhen": {
                "field": "",
                "is": ""
            },
            "required": false,
            "validationRule": "",
            "secure": false
        },
        {
            "element": "textarea",
            "inputType": "",
            "label": "信息",
            "description": "",
            "placeholder": "{{ template \"default.message\" . }}",
            "propertyName": "message",
            "selectOptions": null,
            "showWhen": {
                "field": "",
                "is": ""
            },
            "required": false,
            "validationRule": "",
            "secure": false
        }
    ]
  },
  {
    "type": "slack",
    "name": "Slack",
    "heading": "Slack settings",
    "description": "向Slack发送通知",
    "info": "",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label":"收件人",
        "description": "指定频道或用户，使用#channel-name、@username（必须全部小写，无空格）或user/channel Slack ID-除非您提供了webhook，否则是必需的",
        "placeholder": "",
        "propertyName": "recipient",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "令牌",
        "description": "提供Slack API令牌（以“xoxb”开头）-除非您提供webhook，否则是必需的",
        "propertyName": "token",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": true
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "用户名",
        "description": "设置机器人程序消息的用户名",
        "placeholder": "",
        "propertyName": "username",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "Icon符号",
        "description": "提供一个表情符号作为机器人消息的图标。覆盖图标URL。",
        "placeholder": "",
        "propertyName": "icon_emoji",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label":"Icon地址",
        "description":  "提供图像的URL，用作机器人程序消息的图标",
        "placeholder": "",
        "propertyName": "icon_url",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label":"提及用户",
        "description": "在频道中通知时，按ID提及一个或多个用户（逗号分隔）（您可以从用户的Slack配置文件中复制）",
        "placeholder": "",
        "propertyName": "mentionUsers",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "提及组",
        "description": "在频道中通知时提及一个或多个组（逗号分隔）（您可以从组的Slack配置文件URL中复制）",
        "placeholder": "",
        "propertyName": "mentionGroups",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "select",
        "inputType": "",
        "label": "提及频道",
        "description": "通知时提及整个频道或仅提及活动成员",
        "placeholder": "",
        "propertyName": "mentionChannel",
        "selectOptions": [
          {
            "value": "",
            "label": "禁用"
          },
          {
            "value": "here",
            "label": "每个活跃的通道成员",
          },
          {
            "value": "channel",
            "label": "每个渠道成员",
          }
        ],
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "Webhook地址",
        "description": "可选地提供Slack传入的webhook URL用于发送消息，在这种情况下，不需要令牌",
        "placeholder": "宽松模式传入的Webhook地址",
        "propertyName": "url",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": true
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "标题",
        "description": "闲置邮件的模板标题",
        "placeholder": "{{ template \"slack.default.title\" . }}",
        "propertyName": "title",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "textarea",
        "inputType": "",
        "label": "文本正文",
        "description":  "消息正文",
        "placeholder": "{{ template \"slack.default.text\" . }}",
        "propertyName": "text",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      }
    ]
  },
  {
    "type": "sensugo",
    "name": "Sensu Go",
    "description": "向Sensu Go API发送HTTP POST请求",
	"heading":     "Sensu Go Settings",
    "info": "",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label":  "后端URL",
        "description": "",
        "placeholder": "http://sensu-api.local:8080",
        "propertyName": "url",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "password",
        "label": "API密钥",
        "description": "验证Sensu Go后端的API密钥",
        "placeholder": "",
        "propertyName": "apikey",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": true
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "代理实体名称",
        "description": "",
        "placeholder": "default",
        "propertyName": "entity",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "支票名称",
        "description": "",
        "placeholder": "default",
        "propertyName": "check",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label":"经办人",
        "description": "",
        "placeholder": "",
        "propertyName": "handler",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label":"命名空间",
        "description": "",
        "placeholder": "default",
        "propertyName": "namespace",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "textarea",
        "inputType": "",
        "label":"信息",
        "description": "",
        "placeholder": "{{ template \"default.message\" . }}",
        "propertyName": "message",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      }
    ]
  },
  {
    "type": "teams",
    "name":  "Microsoft团队",
    "heading": "Teams settings",
    "description": "使用传入Webhook连接器向Microsoft Teams发送通知",
    "info": "",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label":"地址",
        "description": "",
        "placeholder":  "团队传入的webhook地址",
        "propertyName": "url",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "textarea",
        "inputType": "",
        "label":"信息",
        "description": "",
        "placeholder": "{{ template \"default.message\" . }}",
        "propertyName": "message",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      }
    ]
  },
  {
    "type": "telegram",
    "name":  "电报",
    "heading": "Telegram API settings",
    "description": "用电报发送通知",
    "info": "",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label": "BOT API令牌",
        "description": "",
        "placeholder": "电报BOT API令牌",
        "propertyName": "bottoken",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": true
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "聊天室",
        "description": "整数电报聊天标识符",
        "placeholder": "",
        "propertyName": "chatid",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "textarea",
        "inputType": "",
        "label":  "信息",
        "description": "",
        "placeholder": "{{ template \"default.message\" . }}",
        "propertyName": "message",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      }
    ]
  },
  {
    "type": "webhook",
    "name": "webhook",
    "heading": "Webhook settings",
    "description":"向地址发送HTTP POST请求",
    "info": "",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label": "地址",
        "description": "",
        "placeholder": "",
        "propertyName": "url",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "select",
        "inputType": "",
        "label":  "Http方法",
        "description": "",
        "placeholder": "",
        "propertyName": "httpMethod",
        "selectOptions": [
          {
            "value": "POST",
            "label": "POST"
          },
          {
            "value": "PUT",
            "label": "PUT"
          }
        ],
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "用户名",
        "description": "",
        "placeholder": "",
        "propertyName": "username",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "password",
        "label":"密码",
        "description": "",
        "placeholder": "",
        "propertyName": "password",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": true
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "最大警报数",
        "description":"通知中包含的最大警报数。超过此数字时，将忽略同一批次中的剩余警报。0表示没有限制。",
        "placeholder": "",
        "propertyName": "maxAlerts",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      }
    ]
  },
  {
    "type": "prometheus-alertmanager",
    "name": "警报管理",
    "heading": "Alertmanager Settings",
    "description": "向管理员发送通知",
    "info": "",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label":  "地址",
        "description": "",
	"placeholder": "http://localhost:9093",
        "propertyName": "url",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "基础身份验证用户",
        "description": "",
	"placeholder": "",
        "propertyName": "basicAuthUser",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "password",
        "label": "基础身份验证密码",
        "description": "",
	"placeholder": "",
        "propertyName": "basicAuthPassword",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": true
      }
    ]
  },
  {
	"type": "discord",
	"name": "Discord",
	"heading": "Discord settings",
	"description": "向Discord发送通知",
	"info": "",
	"options": [
      {
		"label":  "消息内容",
		"description": "提及使用@的群组或使用\u003c@ID\u003e在通道中通知时",
		"element": "input",
		"inputType": "text",
		"placeholder": "{{ template \"default.message\" . }}",
		"propertyName": "message",
		"selectOptions": null,
		"showWhen": {
		  "field": "",
		  "is": ""
		},
		"required": false,
		"validationRule": "",
		"secure": false
	  },
	  {
		"label":  "Webhook地址",
		"description": "",
		"element": "input",
		"inputType": "text",
		"placeholder":"Discord webhook地址",
		"propertyName": "url",
		"selectOptions": null,
		"showWhen": {
		  "field": "",
		  "is": ""
		},
		"required": true,
		"validationRule": "",
		"secure": false
	  },
	  {
		"label": "头像URL",
		"description": "",
		"element": "input",
		"inputType": "text",
		"placeholder": "",
		"propertyName": "avatar_url",
		"selectOptions": null,
		"showWhen": {
		  "field": "",
		  "is": ""
		},
		"required": false,
		"validationRule": "",
		"secure": false
	  }
	]
  },
  {
    "type": "googlechat",
    "name": "Google Hangouts Chat",
    "heading": "Google Hangouts Chat settings",
    "description": "基于官方JSON消息格式，通过Webhook向Google Hangouts Chat发送通知",
    "info": "",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label": "地址",
        "description": "",
        "placeholder": "Google Hangouts聊天传入的webhook url",
        "propertyName": "url",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": false
      }
    ]
  },
  {
    "type": "LINE",
    "name": "LINE",
    "heading": "LINE notify settings",
    "description": "向LINE通知发送通知",
    "info": "",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label":"令牌",
        "description": "",
        "placeholder": "LINE通知令牌密钥",
        "propertyName": "token",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": true
      }
    ]
  },
  {
    "type": "threema",
    "name": "Threema网关",
    "heading": "Threema Gateway settings",
    "description": "使用Threema网关（基本ID）向Threema发送通知",
    "info": "可以为“基本”类型的任何Threema网关ID配置通知。当前不支持端到端ID。可以在上设置Threema网关IDhttps://gateway.threema.ch/.",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label": "网关ID",
        "description": "您的8个字符的Threema网关基本ID（以*开头）。",
        "placeholder": "*3MAGWID",
        "propertyName": "gateway_id",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "\\*[0-9A-Z]{7}",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "收件人ID",
        "description": "应该接收警报的8个字符的Threema ID。",
        "placeholder": "YOUR3MID",
        "propertyName": "recipient_id",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "[0-9A-Z]{8}",
        "secure": false
      },
      {
        "element": "input",
        "inputType": "text",
        "label": "API秘诀",
        "description": "您的Threema Gateway API秘诀",
        "placeholder": "",
        "propertyName": "api_secret",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": true
      }
    ]
  },
  {
    "type": "opsgenie",
    "name": "OpsGenie",
    "heading": "OpsGenie settings",
    "description": "向OpsGenie发送通知",
    "info": "",
    "options": [
      {
        "element": "input",
        "inputType": "text",
        "label": "API密钥",
        "description": "",
        "placeholder": "OpsGenie API密钥",
        "propertyName": "apiKey",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": true
      },
      {
        "element": "input",
        "inputType": "text",
        "label":"警报API地址",
        "description": "",
        "placeholder": "https://api.opsgenie.com/v2/alerts",
        "propertyName": "apiUrl",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": true,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "checkbox",
        "inputType": "",
        "label":  "自动关闭事件",
        "description": "警报恢复正常后，自动关闭OpsGenie中的警报。",
        "placeholder": "",
        "propertyName": "autoClose",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "checkbox",
        "inputType": "",
        "label":  "覆盖优先级",
        "description": "允许使用og_priority标记设置警报优先级",
        "placeholder": "",
        "propertyName": "overridePriority",
        "selectOptions": null,
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      },
      {
        "element": "select",
        "inputType": "",
        "label":  "将通知标记发送",
        "description": "将通用注释作为“额外属性”、“标记”或两者都发送给Opsgenie",
        "placeholder": "",
        "propertyName": "sendTagsAs",
        "selectOptions": [
          {
            "value": "tags",
            "label": "标签"
          },
          {
            "value": "details",
            "label": "额外属性"
          },
          {
            "value": "both",
            "label": "标记和额外属性"
          }
        ],
        "showWhen": {
          "field": "",
          "is": ""
        },
        "required": false,
        "validationRule": "",
        "secure": false
      }
    ]
  }
]
`
