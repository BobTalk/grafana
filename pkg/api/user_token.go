package api

import (
	"context"
	"errors"
	"time"

	"github.com/grafana/grafana/pkg/api/dtos"
	"github.com/grafana/grafana/pkg/api/response"
	"github.com/grafana/grafana/pkg/bus"
	"github.com/grafana/grafana/pkg/models"
	"github.com/grafana/grafana/pkg/util"
	"github.com/ua-parser/uap-go/uaparser"
)

// GET /api/user/auth-tokens
func (hs *HTTPServer) GetUserAuthTokens(c *models.ReqContext) response.Response {
	return hs.getUserAuthTokensInternal(c, c.UserId)
}

// POST /api/user/revoke-auth-token
func (hs *HTTPServer) RevokeUserAuthToken(c *models.ReqContext, cmd models.RevokeAuthTokenCmd) response.Response {
	return hs.revokeUserAuthTokenInternal(c, c.UserId, cmd)
}

func (hs *HTTPServer) logoutUserFromAllDevicesInternal(ctx context.Context, userID int64) response.Response {
	userQuery := models.GetUserByIdQuery{Id: userID}

	if err := bus.Dispatch(&userQuery); err != nil {
		if errors.Is(err, models.ErrUserNotFound) {
			return response.Error(404, "未找到用户", err)
		}
		return response.Error(500, "无法从数据库中读取用户", err)
	}

	err := hs.AuthTokenService.RevokeAllUserTokens(ctx, userID)
	if err != nil {
		return response.Error(500, "注销用户失败", err)
	}

	return response.JSON(200, util.DynMap{
		"message": "用户已注销",
	})
}

func (hs *HTTPServer) getUserAuthTokensInternal(c *models.ReqContext, userID int64) response.Response {
	userQuery := models.GetUserByIdQuery{Id: userID}

	if err := bus.Dispatch(&userQuery); err != nil {
		if errors.Is(err, models.ErrUserNotFound) {
			return response.Error(404, "未找到用户", err)
		}
		return response.Error(500, "无法获取用户", err)
	}

	tokens, err := hs.AuthTokenService.GetUserTokens(c.Req.Context(), userID)
	if err != nil {
		return response.Error(500, "无法获取用户身份验证令牌", err)
	}

	result := []*dtos.UserToken{}
	for _, token := range tokens {
		isActive := false
		if c.UserToken != nil && c.UserToken.Id == token.Id {
			isActive = true
		}

		parser := uaparser.NewFromSaved()
		client := parser.Parse(token.UserAgent)

		osVersion := ""
		if client.Os.Major != "" {
			osVersion = client.Os.Major

			if client.Os.Minor != "" {
				osVersion = osVersion + "." + client.Os.Minor
			}
		}

		browserVersion := ""
		if client.UserAgent.Major != "" {
			browserVersion = client.UserAgent.Major

			if client.UserAgent.Minor != "" {
				browserVersion = browserVersion + "." + client.UserAgent.Minor
			}
		}

		createdAt := time.Unix(token.CreatedAt, 0)
		seenAt := time.Unix(token.SeenAt, 0)

		if token.SeenAt == 0 {
			seenAt = createdAt
		}

		result = append(result, &dtos.UserToken{
			Id:                     token.Id,
			IsActive:               isActive,
			ClientIp:               token.ClientIp,
			Device:                 client.Device.ToString(),
			OperatingSystem:        client.Os.Family,
			OperatingSystemVersion: osVersion,
			Browser:                client.UserAgent.Family,
			BrowserVersion:         browserVersion,
			CreatedAt:              createdAt,
			SeenAt:                 seenAt,
		})
	}

	return response.JSON(200, result)
}

func (hs *HTTPServer) revokeUserAuthTokenInternal(c *models.ReqContext, userID int64, cmd models.RevokeAuthTokenCmd) response.Response {
	userQuery := models.GetUserByIdQuery{Id: userID}

	if err := bus.Dispatch(&userQuery); err != nil {
		if errors.Is(err, models.ErrUserNotFound) {
			return response.Error(404, "未找到用户", err)
		}
		return response.Error(500, "无法获取用户", err)
	}

	token, err := hs.AuthTokenService.GetUserToken(c.Req.Context(), userID, cmd.AuthTokenId)
	if err != nil {
		if errors.Is(err, models.ErrUserTokenNotFound) {
			return response.Error(404, "找不到用户身份验证令牌", err)
		}
		return response.Error(500, "无法获取用户身份验证令牌", err)
	}

	if c.UserToken != nil && c.UserToken.Id == token.Id {
		return response.Error(400, "无法吊销活动用户身份验证令牌", nil)
	}

	err = hs.AuthTokenService.RevokeToken(c.Req.Context(), token, false)
	if err != nil {
		if errors.Is(err, models.ErrUserTokenNotFound) {
			return response.Error(404, "找不到用户身份验证令牌", err)
		}
		return response.Error(500, "无法吊销用户身份验证令牌", err)
	}

	return response.JSON(200, util.DynMap{
		"message": "已吊销用户身份验证令牌",
	})
}
