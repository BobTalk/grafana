package notifier

import (
	"github.com/grafana/grafana/pkg/services/alerting"
	"github.com/grafana/grafana/pkg/services/ngalert/notifier/channels"
)

// GetAvailableNotifiers returns the metadata of all the notification channels that can be configured.
func GetAvailableNotifiers() []*alerting.NotifierPlugin {
	pushoverSoundOptions := []alerting.SelectOption{
		{
			Value: "default",
			Label: "默认",
		},
		{
			Value: "pushover",
			Label: "推送",
		}, {
			Value: "bike",
			Label: "自行车",
		}, {
			Value: "bugle",
			Label: "军号",
		}, {
			Value: "cashregister",
			Label: "收银机",
		}, {
			Value: "classical",
			Label: "古典",
		}, {
			Value: "cosmic",
			Label: "宇宙",
		}, {
			Value: "falling",
			Label: "坠落",
		}, {
			Value: "gamelan",
			Label: "Gamelan",
		}, {
			Value: "incoming",
			Label: "接收",
		}, {
			Value: "intermission",
			Label: "间歇",
		}, {
			Value: "magic",
			Label: "魔术",
		}, {
			Value: "mechanical",
			Label: "机械",
		}, {
			Value: "pianobar",
			Label: "钢琴酒吧",
		}, {
			Value: "siren",
			Label: "汽笛",
		}, {
			Value: "spacealarm",
			Label: "空间报警器",
		}, {
			Value: "tugboat",
			Label: "拖船",
		}, {
			Value: "alien",
			Label: "外星人",
		}, {
			Value: "climb",
			Label: "攀登",
		}, {
			Value: "persistent",
			Label: "持久",
		}, {
			Value: "echo",
			Label: "回响",
		}, {
			Value: "updown",
			Label: "七上八下",
		}, {
			Value: "none",
			Label: "无",
		},
	}

	pushoverPriorityOptions := []alerting.SelectOption{
		{
			Value: "2",
			Label: "紧急",
		},
		{
			Value: "1",
			Label: "高",
		},
		{
			Value: "0",
			Label: "普通",
		},
		{
			Value: "-1",
			Label: "低",
		},
		{
			Value: "-2",
			Label: "最低",
		},
	}

	return []*alerting.NotifierPlugin{
		{
			Type:        "dingding",
			Name:        "钉钉",
			Description: "向钉钉发送HTTP POST请求",
			Heading:     "DingDing settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "地址",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "https://oapi.dingtalk.com/robot/send?access_token=xxxxxxxxx",
					PropertyName: "url",
					Required:     true,
				},
				{
					Label:        "消息类型",
					Element:      alerting.ElementTypeSelect,
					PropertyName: "msgType",
					SelectOptions: []alerting.SelectOption{
						{
							Value: "链接",
							Label: "Link"},
						{
							Value: "actionCard",
							Label: "行动卡",
						},
					},
				},
				{ // New in 8.0.
					Label:        "信息",
					Element:      alerting.ElementTypeTextArea,
					Placeholder:  `{{ template "default.message" . }}`,
					PropertyName: "message",
				},
			},
		},
		{
			Type:        "kafka",
			Name:        "Kafka REST代理",
			Description: "向Kafka Rest代理发送通知",
			Heading:     "Kafka settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "Kafka REST代理",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "http://localhost:8082",
					PropertyName: "kafkaRestProxy",
					Required:     true,
				},
				{
					Label:        "话题",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "话题",
					PropertyName: "kafkaTopic",
					Required:     true,
				},
			},
		},
		{
			Type:        "email",
			Name:        "邮件",
			Description: "使用Grafana服务器配置的SMTP设置发送通知",
			Heading:     "邮件设置",
			Options: []alerting.NotifierOption{
				{
					Label:        "单邮件",
					Description:  "向所有收件人发送一封电子邮件",
					Element:      alerting.ElementTypeCheckbox,
					PropertyName: "singleEmail",
				},
				{
					Label:        "地址",
					Description:  "您可以使用“；”分隔符输入多个电子邮件地址",
					Element:      alerting.ElementTypeTextArea,
					PropertyName: "addresses",
					Required:     true,
				},
				{ // New in 8.0.
					Label:        "信息",
					Description:  "要包含在电子邮件中的可选消息,可以使用模板变量",
					Element:      alerting.ElementTypeTextArea,
					PropertyName: "message",
				},
			},
		},
		{
			Type:        "pagerduty",
			Name:        "PagerDuty",
			Description: "向PagerDuty发送通知",
			Heading:     "PagerDuty settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "Integration密钥",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "Pagerduty集成密钥",
					PropertyName: "integrationKey",
					Required:     true,
					Secure:       true,
				},
				{
					Label:   "程度",
					Element: alerting.ElementTypeSelect,
					SelectOptions: []alerting.SelectOption{
						{
							Value: "critical",
							Label: "严重",
						},
						{
							Value: "error",
							Label: "错误",
						},
						{
							Value: "warning",
							Label: "警告",
						},
						{
							Value: "info",
							Label: "信息",
						},
					},
					PropertyName: "severity",
				},
				{ // New in 8.0.
					Label:        "类/类型",
					Description:  "事件的类/类型，例如“ping failure”或“cpu load”",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					PropertyName: "class",
				},
				{ // New in 8.0.
					Label:        "Component",
					Description:  "负责事件的源机器的组件，例如mysql或eth0",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "Grafana",
					PropertyName: "component",
				},
				{ // New in 8.0.
					Label:        "Group",
					Description:  "服务组件的逻辑分组，例如“应用程序堆栈”",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					PropertyName: "group",
				},
				{ // New in 8.0.
					Label:        "Summary",
					Description:  "您可以使用模板进行摘要",
					Element:      alerting.ElementTypeTextArea,
					Placeholder:  `{{ template "default.message" . }}`,
					PropertyName: "summary",
				},
			},
		},
		{
			Type:        "victorops",
			Name:        "VictorOps",
			Description: "向VictorOps发送通知",
			Heading:     "VictorOps settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "地址",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "VictorOps地址",
					PropertyName: "url",
					Required:     true,
				},
				{ // New in 8.0.
					Label:        "消息类型",
					Element:      alerting.ElementTypeSelect,
					PropertyName: "messageType",
					SelectOptions: []alerting.SelectOption{
						{
							Value: "CRITICAL",
							Label: "严重"},
						{
							Value: "WARNING",
							Label: "警告",
						},
					},
				},
			},
		},
		{
			Type:        "pushover",
			Name:        "Pushover",
			Description: "向Pushover API发送HTTP POST请求",
			Heading:     "Pushover settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "API令牌",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "应用程序令牌",
					PropertyName: "apiToken",
					Required:     true,
					Secure:       true,
				},
				{
					Label:        "用户密钥",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "逗号分隔列表",
					PropertyName: "userKey",
					Required:     true,
					Secure:       true,
				},
				{
					Label:        "设备（可选）",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "逗号分隔列表；保留为空以发送到所有设备",
					PropertyName: "device",
				},
				{
					Label:         "警报优先级",
					Element:       alerting.ElementTypeSelect,
					SelectOptions: pushoverPriorityOptions,
					PropertyName:  "priority",
				},
				{
					Label:         "OK优先级",
					Element:       alerting.ElementTypeSelect,
					SelectOptions: pushoverPriorityOptions,
					PropertyName:  "okPriority",
				},
				{
					Description:  "Pushover服务器向用户发送相同警报或OK通知的频率（以秒为单位）。",
					Label:        "重试（仅用于紧急优先级）",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "至少30秒",
					PropertyName: "retry",
				},
				{
					Description:  "将继续重试警报或OK通知的秒数。",
					Label:        "过期（仅用于紧急优先级）",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "最长86400秒",
					PropertyName: "expire",
				},
				{
					Label:         "报警声音",
					Element:       alerting.ElementTypeSelect,
					SelectOptions: pushoverSoundOptions,
					PropertyName:  "sound",
				},
				{
					Label:         "OK声音",
					Element:       alerting.ElementTypeSelect,
					SelectOptions: pushoverSoundOptions,
					PropertyName:  "okSound",
				},
				{ // New in 8.0.
					Label:        "信息",
					Element:      alerting.ElementTypeTextArea,
					Placeholder:  `{{ template "default.message" . }}`,
					PropertyName: "message",
				},
			},
		},
		{
			Type:        "slack",
			Name:        "Slack",
			Description: "向Slack发送通知",
			Heading:     "Slack settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "收件人",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Description:  "指定频道或用户，使用#channel-name、@username（必须全部小写，无空格）或user/channel Slack ID-除非您提供了webhook，否则是必需的",
					PropertyName: "recipient",
				},
				// Logically, this field should be required when not using a webhook, since the Slack API needs a token.
				// However, since the UI doesn't allow to say that a field is required or not depending on another field,
				// we've gone with the compromise of making this field optional and instead return a validation error
				// if it's necessary and missing.
				{
					Label:        "令牌",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Description:  "提供Slack API令牌（以“xoxb”开头）-除非您提供webhook，否则是必需的",
					PropertyName: "token",
					Secure:       true,
				},
				{
					Label:        "用户名",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Description:  "设置机器人程序消息的用户名",
					PropertyName: "username",
				},
				{
					Label:        "Icon符号",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Description:  "提供一个表情符号作为机器人消息的图标。覆盖图标URL。",
					PropertyName: "icon_emoji",
				},
				{
					Label:        "Icon地址",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Description:  "提供图像的URL，用作机器人程序消息的图标",
					PropertyName: "icon_url",
				},
				{
					Label:        "提及用户",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Description:  "在频道中通知时，按ID提及一个或多个用户（逗号分隔）（您可以从用户的Slack配置文件中复制）",
					PropertyName: "mentionUsers",
				},
				{
					Label:        "提及组",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Description:  "在频道中通知时提及一个或多个组（逗号分隔）（您可以从组的Slack配置文件URL中复制）",
					PropertyName: "mentionGroups",
				},
				{
					Label:   "提及频道",
					Element: alerting.ElementTypeSelect,
					SelectOptions: []alerting.SelectOption{
						{
							Value: "",
							Label: "禁用",
						},
						{
							Value: "here",
							Label: "每个活跃的通道成员",
						},
						{
							Value: "channel",
							Label: "每个渠道成员",
						},
					},
					Description:  "通知时提及整个频道或仅提及活动成员",
					PropertyName: "mentionChannel",
				},
				{
					Label:        "Webhook地址",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Description:  "可选地提供Slack传入的webhook URL用于发送消息，在这种情况下，不需要令牌",
					Placeholder:  "宽松模式传入的Webhook地址",
					PropertyName: "url",
					Secure:       true,
				},
				{ // New in 8.0.
					Label:        "标题",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Description:  "闲置邮件的模板标题",
					PropertyName: "title",
					Placeholder:  `{{ template "slack.default.title" . }}`,
				},
				{ // New in 8.0.
					Label:        "文本正文",
					Element:      alerting.ElementTypeTextArea,
					Description:  "消息正文",
					PropertyName: "text",
					Placeholder:  `{{ template "slack.default.text" . }}`,
				},
			},
		},
		{
			Type:        "sensugo",
			Name:        "Sensu Go",
			Description: "向Sensu Go API发送HTTP POST请求",
			Heading:     "Sensu Go Settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "后端URL",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "http://sensu-api.local:8080",
					PropertyName: "url",
					Required:     true,
				},
				{
					Label:        "API密钥",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypePassword,
					Description:  "验证Sensu Go后端的API密钥",
					PropertyName: "apikey",
					Required:     true,
					Secure:       true,
				},
				{
					Label:        "代理实体名称",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "default",
					PropertyName: "entity",
				},
				{
					Label:        "支票名称",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "default",
					PropertyName: "check",
				},
				{
					Label:        "经办人",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					PropertyName: "handler",
				},
				{
					Label:        "命名空间",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "default",
					PropertyName: "namespace",
				},
				{ // New in 8.0.
					Label:        "信息",
					Element:      alerting.ElementTypeTextArea,
					Placeholder:  `{{ template "default.message" . }}`,
					PropertyName: "message",
				},
			},
		},
		{
			Type:        "teams",
			Name:        "Microsoft团队",
			Description: "使用传入Webhook连接器向Microsoft Teams发送通知",
			Heading:     "Teams settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "地址",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "团队传入的webhook地址",
					PropertyName: "url",
					Required:     true,
				},
				{ // New in 8.0.
					Label:        "信息",
					Element:      alerting.ElementTypeTextArea,
					Placeholder:  `{{ template "default.message" . }}`,
					PropertyName: "message",
				},
			},
		},
		{
			Type:        "telegram",
			Name:        "电报",
			Description: "用电报发送通知",
			Heading:     "Telegram API settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "BOT API令牌",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "电报BOT API令牌",
					PropertyName: "bottoken",
					Required:     true,
					Secure:       true,
				},
				{
					Label:        "聊天室",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Description:  "整数电报聊天标识符",
					PropertyName: "chatid",
					Required:     true,
				},
				{ // New in 8.0.
					Label:        "信息",
					Element:      alerting.ElementTypeTextArea,
					Placeholder:  `{{ template "default.message" . }}`,
					PropertyName: "message",
				},
			},
		},
		{
			Type:        "webhook",
			Name:        "webhook",
			Description: "向地址发送HTTP POST请求",
			Heading:     "Webhook settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "地址",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					PropertyName: "url",
					Required:     true,
				},
				{
					Label:   "Http方法",
					Element: alerting.ElementTypeSelect,
					SelectOptions: []alerting.SelectOption{
						{
							Value: "POST",
							Label: "POST",
						},
						{
							Value: "PUT",
							Label: "PUT",
						},
					},
					PropertyName: "httpMethod",
				},
				{
					Label:        "用户名",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					PropertyName: "username",
				},
				{
					Label:        "密码",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypePassword,
					PropertyName: "password",
					Secure:       true,
				},
				{ // New in 8.0. TODO: How to enforce only numbers?
					Label:        "最大警报数",
					Description:  "通知中包含的最大警报数。超过此数字时，将忽略同一批次中的剩余警报。0表示没有限制。",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					PropertyName: "maxAlerts",
				},
			},
		},
		{
			Type:        "prometheus-alertmanager",
			Name:        "警报管理",
			Description: "向管理员发送通知",
			Heading:     "Alertmanager Settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "地址",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "http://localhost:9093",
					PropertyName: "url",
					Required:     true,
				},
				{
					Label:        "基础身份验证用户",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					PropertyName: "basicAuthUser",
				},
				{
					Label:        "基础身份验证密码",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypePassword,
					PropertyName: "basicAuthPassword",
					Secure:       true,
				},
			},
		},
		{
			Type:        "discord",
			Name:        "Discord",
			Heading:     "Discord settings",
			Description: "向Discord发送通知",
			Options: []alerting.NotifierOption{
				{
					Label:        "消息内容",
					Description:  "提及使用@的群组或使用\u003c@ID\u003e在通道中通知时",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  `{{ template "default.message" . }}`,
					PropertyName: "message",
				},
				{
					Label:        "Webhook地址",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "Discord webhook地址",
					PropertyName: "url",
					Required:     true,
				},
				{
					Label:        "头像URL",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					PropertyName: "avatar_url",
				},
			},
		},
		{
			Type:        "googlechat",
			Name:        "Google Hangouts Chat",
			Description: "基于官方JSON消息格式，通过Webhook向Google Hangouts Chat发送通知",
			Heading:     "Google Hangouts Chat settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "地址",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "Google Hangouts聊天传入的webhook url",
					PropertyName: "url",
					Required:     true,
				},
			},
		},
		{
			Type:        "LINE",
			Name:        "LINE",
			Description: "向LINE通知发送通知",
			Heading:     "LINE notify settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "令牌",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "LINE通知令牌密钥",
					PropertyName: "token",
					Required:     true,
					Secure:       true,
				}},
		},
		{
			Type:        "threema",
			Name:        "Threema网关",
			Description: "使用Threema网关（基本ID）向Threema发送通知",
			Heading:     "Threema Gateway settings",
			Info:        "可以为“基本”类型的任何Threema网关ID配置通知。当前不支持端到端ID。可以在上设置Threema网关IDhttps://gateway.threema.ch/.",
			Options: []alerting.NotifierOption{
				{
					Label:          "网关ID",
					Element:        alerting.ElementTypeInput,
					InputType:      alerting.InputTypeText,
					Placeholder:    "*3MAGWID",
					Description:    "您的8个字符的Threema网关基本ID（以*开头）。",
					PropertyName:   "gateway_id",
					Required:       true,
					ValidationRule: "\\*[0-9A-Z]{7}",
				},
				{
					Label:          "收件人ID",
					Element:        alerting.ElementTypeInput,
					InputType:      alerting.InputTypeText,
					Placeholder:    "YOUR3MID",
					Description:    "应该接收警报的8个字符的Threema ID。",
					PropertyName:   "recipient_id",
					Required:       true,
					ValidationRule: "[0-9A-Z]{8}",
				},
				{
					Label:        "API秘诀",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Description:  "您的Threema Gateway API秘诀",
					PropertyName: "api_secret",
					Required:     true,
					Secure:       true,
				},
			},
		},
		{
			Type:        "opsgenie",
			Name:        "OpsGenie",
			Description: "向OpsGenie发送通知",
			Heading:     "OpsGenie settings",
			Options: []alerting.NotifierOption{
				{
					Label:        "API密钥",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "OpsGenie API密钥",
					PropertyName: "apiKey",
					Required:     true,
					Secure:       true,
				},
				{
					Label:        "警报API地址",
					Element:      alerting.ElementTypeInput,
					InputType:    alerting.InputTypeText,
					Placeholder:  "https://api.opsgenie.com/v2/alerts",
					PropertyName: "apiUrl",
					Required:     true,
				},
				{
					Label:        "自动关闭事件",
					Element:      alerting.ElementTypeCheckbox,
					Description:  "警报恢复正常后，自动关闭OpsGenie中的警报。",
					PropertyName: "autoClose",
				}, {
					Label:        "覆盖优先级",
					Element:      alerting.ElementTypeCheckbox,
					Description:  "允许使用og_priority标记设置警报优先级",
					PropertyName: "overridePriority",
				},
				{
					Label:   "将通知标记发送",
					Element: alerting.ElementTypeSelect,
					SelectOptions: []alerting.SelectOption{
						{
							Value: channels.OpsgenieSendTags,
							Label: "标签",
						},
						{
							Value: channels.OpsgenieSendDetails,
							Label: "额外属性",
						},
						{
							Value: channels.OpsgenieSendBoth,
							Label: "标记和额外属性",
						},
					},
					Description:  "将通用注释作为“额外属性”、“标记”或两者都发送给Opsgenie",
					PropertyName: "sendTagsAs",
				},
			},
		},
	}
}
