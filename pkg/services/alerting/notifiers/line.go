/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 11:23:13
 * @FilePath: /grafana/pkg/services/alerting/notifiers/line.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package notifiers

import (
	"fmt"
	"net/url"

	"github.com/grafana/grafana/pkg/bus"
	"github.com/grafana/grafana/pkg/infra/log"
	"github.com/grafana/grafana/pkg/models"
	"github.com/grafana/grafana/pkg/services/alerting"
)

func init() {
	alerting.RegisterNotifier(&alerting.NotifierPlugin{
		Type:        "LINE",
		Name:        "LINE",
		Description: "向LINE通知发送通知",
		Heading:     "LINE notify settings",
		Factory:     NewLINENotifier,
		Options: []alerting.NotifierOption{
			{
				Label:        "令牌",
				Element:      alerting.ElementTypeInput,
				InputType:    alerting.InputTypeText,
				Placeholder:  "LINE通知令牌密钥",
				PropertyName: "token",
				Required:     true,
				Secure:       true,
			}},
	})
}

const (
	lineNotifyURL string = "https://notify-api.line.me/api/notify"
)

// NewLINENotifier is the constructor for the LINE notifier
func NewLINENotifier(model *models.AlertNotification) (alerting.Notifier, error) {
	token := model.DecryptedValue("token", model.Settings.Get("token").MustString())
	if token == "" {
		return nil, alerting.ValidationError{Reason: "Could not find token in settings"}
	}

	return &LineNotifier{
		NotifierBase: NewNotifierBase(model),
		Token:        token,
		log:          log.New("alerting.notifier.line"),
	}, nil
}

// LineNotifier is responsible for sending
// alert notifications to LINE.
type LineNotifier struct {
	NotifierBase
	Token string
	log   log.Logger
}

// Notify send an alert notification to LINE
func (ln *LineNotifier) Notify(evalContext *alerting.EvalContext) error {
	ln.log.Info("Executing line notification", "ruleId", evalContext.Rule.ID, "notification", ln.Name)

	return ln.createAlert(evalContext)
}

func (ln *LineNotifier) createAlert(evalContext *alerting.EvalContext) error {
	ln.log.Info("Creating Line notify", "ruleId", evalContext.Rule.ID, "notification", ln.Name)
	ruleURL, err := evalContext.GetRuleURL()
	if err != nil {
		ln.log.Error("Failed get rule link", "error", err)
		return err
	}

	form := url.Values{}
	body := fmt.Sprintf("%s - %s\n%s", evalContext.GetNotificationTitle(), ruleURL, evalContext.Rule.Message)
	form.Add("message", body)

	if ln.NeedsImage() && evalContext.ImagePublicURL != "" {
		form.Add("imageThumbnail", evalContext.ImagePublicURL)
		form.Add("imageFullsize", evalContext.ImagePublicURL)
	}

	cmd := &models.SendWebhookSync{
		Url:        lineNotifyURL,
		HttpMethod: "POST",
		HttpHeader: map[string]string{
			"Authorization": fmt.Sprintf("Bearer %s", ln.Token),
			"Content-Type":  "application/x-www-form-urlencoded;charset=UTF-8",
		},
		Body: form.Encode(),
	}

	if err := bus.DispatchCtx(evalContext.Ctx, cmd); err != nil {
		ln.log.Error("Failed to send notification to LINE", "error", err, "body", body)
		return err
	}

	return nil
}
