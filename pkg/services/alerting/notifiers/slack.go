package notifiers

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/grafana/grafana/pkg/bus"
	"github.com/grafana/grafana/pkg/infra/log"
	"github.com/grafana/grafana/pkg/models"
	"github.com/grafana/grafana/pkg/services/alerting"
	"github.com/grafana/grafana/pkg/setting"
)

func init() {
	alerting.RegisterNotifier(&alerting.NotifierPlugin{
		Type:        "slack",
		Name:        "Slack",
		Description: "向Slack发送通知",
		Heading:     "Slack settings",
		Factory:     NewSlackNotifier,
		Options: []alerting.NotifierOption{
			{
				Label:        "收件人",
				Element:      alerting.ElementTypeInput,
				InputType:    alerting.InputTypeText,
				Description:  "指定频道或用户，使用#channel-name、@username（必须全部小写，无空格）或user/channel Slack ID-除非您提供了webhook，否则是必需的",
				PropertyName: "recipient",
			},
			// Logically, this field should be required when not using a webhook, since the Slack API needs a token.
			// However, since the UI doesn't allow to say that a field is required or not depending on another field,
			// we've gone with the compromise of making this field optional and instead return a validation error
			// if it's necessary and missing.
			{
				Label:        "令牌",
				Element:      alerting.ElementTypeInput,
				InputType:    alerting.InputTypeText,
				Description:  "提供Slack API令牌（以“xoxb”开头）-除非您提供webhook，否则是必需的",
				PropertyName: "token",
				Secure:       true,
			},
			{
				Label:        "用户名",
				Element:      alerting.ElementTypeInput,
				InputType:    alerting.InputTypeText,
				Description:  "设置机器人程序消息的用户名",
				PropertyName: "username",
			},
			{
				Label:        "Icon符号",
				Element:      alerting.ElementTypeInput,
				InputType:    alerting.InputTypeText,
				Description:  "提供一个表情符号作为机器人消息的图标。覆盖图标URL。",
				PropertyName: "iconEmoji",
			},
			{
				Label:        "Icon地址",
				Element:      alerting.ElementTypeInput,
				InputType:    alerting.InputTypeText,
				Description:  "提供图像的URL，用作机器人程序消息的图标",
				PropertyName: "iconUrl",
			},
			{
				Label:        "提及用户",
				Element:      alerting.ElementTypeInput,
				InputType:    alerting.InputTypeText,
				Description:  "在频道中通知时，按ID提及一个或多个用户（逗号分隔）（您可以从用户的Slack配置文件中复制）",
				PropertyName: "mentionUsers",
			},
			{
				Label:        "提及组",
				Element:      alerting.ElementTypeInput,
				InputType:    alerting.InputTypeText,
				Description:  "在频道中通知时提及一个或多个组（逗号分隔）（您可以从组的Slack配置文件URL中复制）",
				PropertyName: "mentionGroups",
			},
			{
				Label:   "提及频道",
				Element: alerting.ElementTypeSelect,
				SelectOptions: []alerting.SelectOption{
					{
						Value: "",
						Label: "禁用",
					},
					{
						Value: "here",
						Label: "每个活跃的通道成员",
					},
					{
						Value: "channel",
						Label: "每个渠道成员",
					},
				},
				Description:  "通知时提及整个频道或仅提及活动成员",
				PropertyName: "mentionChannel",
			},
			{
				Label:        "Webhook地址",
				Element:      alerting.ElementTypeInput,
				InputType:    alerting.InputTypeText,
				Description:  "可选地提供Slack传入的webhook URL用于发送消息，在这种情况下，不需要令牌",
				Placeholder:  "宽松模式传入的Webhook地址",
				PropertyName: "url",
				Secure:       true,
			},
		},
	})
}

var reRecipient *regexp.Regexp = regexp.MustCompile("^((@[a-z0-9][a-zA-Z0-9._-]*)|(#[^ .A-Z]{1,79})|([a-zA-Z0-9]+))$")

const slackAPIEndpoint = "https://slack.com/api/chat.postMessage"

// NewSlackNotifier is the constructor for the Slack notifier.
func NewSlackNotifier(model *models.AlertNotification) (alerting.Notifier, error) {
	urlStr := model.DecryptedValue("url", model.Settings.Get("url").MustString())
	if urlStr == "" {
		urlStr = slackAPIEndpoint
	}
	apiURL, err := url.Parse(urlStr)
	if err != nil {
		return nil, fmt.Errorf("invalid URL %q: %w", urlStr, err)
	}

	recipient := strings.TrimSpace(model.Settings.Get("recipient").MustString())
	if recipient != "" {
		if !reRecipient.MatchString(recipient) {
			return nil, alerting.ValidationError{Reason: fmt.Sprintf("recipient on invalid format: %q", recipient)}
		}
	} else if apiURL.String() == slackAPIEndpoint {
		return nil, alerting.ValidationError{
			Reason: "recipient must be specified when using the Slack chat API",
		}
	}
	username := model.Settings.Get("username").MustString()
	iconEmoji := model.Settings.Get("icon_emoji").MustString()
	iconURL := model.Settings.Get("icon_url").MustString()
	mentionUsersStr := model.Settings.Get("mentionUsers").MustString()
	mentionGroupsStr := model.Settings.Get("mentionGroups").MustString()
	mentionChannel := model.Settings.Get("mentionChannel").MustString()
	token := model.DecryptedValue("token", model.Settings.Get("token").MustString())
	if token == "" && apiURL.String() == slackAPIEndpoint {
		return nil, alerting.ValidationError{
			Reason: "token must be specified when using the Slack chat API",
		}
	}

	uploadImage := model.Settings.Get("uploadImage").MustBool(true)

	if mentionChannel != "" && mentionChannel != "here" && mentionChannel != "channel" {
		return nil, alerting.ValidationError{
			Reason: fmt.Sprintf("Invalid value for mentionChannel: %q", mentionChannel),
		}
	}
	mentionUsers := []string{}
	for _, u := range strings.Split(mentionUsersStr, ",") {
		u = strings.TrimSpace(u)
		if u != "" {
			mentionUsers = append(mentionUsers, u)
		}
	}
	mentionGroups := []string{}
	for _, g := range strings.Split(mentionGroupsStr, ",") {
		g = strings.TrimSpace(g)
		if g != "" {
			mentionGroups = append(mentionGroups, g)
		}
	}

	return &SlackNotifier{
		url:            apiURL,
		NotifierBase:   NewNotifierBase(model),
		recipient:      recipient,
		username:       username,
		iconEmoji:      iconEmoji,
		iconURL:        iconURL,
		mentionUsers:   mentionUsers,
		mentionGroups:  mentionGroups,
		mentionChannel: mentionChannel,
		token:          token,
		upload:         uploadImage,
		log:            log.New("alerting.notifier.slack"),
	}, nil
}

// SlackNotifier is responsible for sending
// alert notification to Slack.
type SlackNotifier struct {
	NotifierBase
	url            *url.URL
	recipient      string
	username       string
	iconEmoji      string
	iconURL        string
	mentionUsers   []string
	mentionGroups  []string
	mentionChannel string
	token          string
	upload         bool
	log            log.Logger
}

// Notify sends an alert notification to Slack.
func (sn *SlackNotifier) Notify(evalContext *alerting.EvalContext) error {
	sn.log.Info("Executing slack notification", "ruleId", evalContext.Rule.ID, "notification", sn.Name)

	ruleURL, err := evalContext.GetRuleURL()
	if err != nil {
		sn.log.Error("Failed to get rule link", "error", err)
		return err
	}

	fields := make([]map[string]interface{}, 0)
	for _, evt := range evalContext.EvalMatches {
		fields = append(fields, map[string]interface{}{
			"title": evt.Metric,
			"value": evt.Value,
			"short": true,
		})
	}

	if evalContext.Error != nil {
		fields = append(fields, map[string]interface{}{
			"title": "Error message",
			"value": evalContext.Error.Error(),
			"short": false,
		})
	}

	mentionsBuilder := strings.Builder{}
	appendSpace := func() {
		if mentionsBuilder.Len() > 0 {
			mentionsBuilder.WriteString(" ")
		}
	}
	mentionChannel := strings.TrimSpace(sn.mentionChannel)
	if mentionChannel != "" {
		mentionsBuilder.WriteString(fmt.Sprintf("<!%s|%s>", mentionChannel, mentionChannel))
	}
	if len(sn.mentionGroups) > 0 {
		appendSpace()
		for _, g := range sn.mentionGroups {
			mentionsBuilder.WriteString(fmt.Sprintf("<!subteam^%s>", g))
		}
	}
	if len(sn.mentionUsers) > 0 {
		appendSpace()
		for _, u := range sn.mentionUsers {
			mentionsBuilder.WriteString(fmt.Sprintf("<@%s>", u))
		}
	}
	msg := ""
	if evalContext.Rule.State != models.AlertStateOK { // don't add message when going back to alert state ok.
		msg = evalContext.Rule.Message
	}
	imageURL := ""
	// default to file.upload API method if a token is provided
	if sn.token == "" {
		imageURL = evalContext.ImagePublicURL
	}

	var blocks []map[string]interface{}
	if mentionsBuilder.Len() > 0 {
		blocks = []map[string]interface{}{
			{
				"type": "section",
				"text": map[string]interface{}{
					"type": "mrkdwn",
					"text": mentionsBuilder.String(),
				},
			},
		}
	}
	attachment := map[string]interface{}{
		"color":       evalContext.GetStateModel().Color,
		"title":       evalContext.GetNotificationTitle(),
		"title_link":  ruleURL,
		"text":        msg,
		"fallback":    evalContext.GetNotificationTitle(),
		"fields":      fields,
		"footer":      "Grafana v" + setting.BuildVersion,
		"footer_icon": "https://grafana.com/assets/img/fav32.png",
		"ts":          time.Now().Unix(),
	}
	if sn.NeedsImage() && imageURL != "" {
		attachment["image_url"] = imageURL
	}
	body := map[string]interface{}{
		"channel": sn.recipient,
		"text":    evalContext.GetNotificationTitle(),
		"attachments": []map[string]interface{}{
			attachment,
		},
	}
	if len(blocks) > 0 {
		body["blocks"] = blocks
	}

	if sn.username != "" {
		body["username"] = sn.username
	}
	if sn.iconEmoji != "" {
		body["icon_emoji"] = sn.iconEmoji
	}
	if sn.iconURL != "" {
		body["icon_url"] = sn.iconURL
	}
	data, err := json.Marshal(&body)
	if err != nil {
		return err
	}

	if err := sn.sendRequest(evalContext.Ctx, data); err != nil {
		return err
	}

	if sn.token != "" && sn.UploadImage {
		err := sn.slackFileUpload(evalContext, sn.log, sn.recipient, sn.token)
		if err != nil {
			return err
		}
	}

	return nil
}

func (sn *SlackNotifier) sendRequest(ctx context.Context, data []byte) error {
	sn.log.Debug("Sending Slack API request", "url", sn.url.String(), "data", string(data))
	request, err := http.NewRequestWithContext(ctx, http.MethodPost, sn.url.String(), bytes.NewReader(data))
	if err != nil {
		return fmt.Errorf("failed to create HTTP request: %w", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("User-Agent", "Grafana")
	if sn.token == "" {
		if sn.url.String() == slackAPIEndpoint {
			panic("Token should be set when using the Slack chat API")
		}
	} else {
		sn.log.Debug("Adding authorization header to HTTP request")
		request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", sn.token))
	}

	netTransport := &http.Transport{
		TLSClientConfig: &tls.Config{
			Renegotiation: tls.RenegotiateFreelyAsClient,
		},
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout: 30 * time.Second,
		}).DialContext,
		TLSHandshakeTimeout: 5 * time.Second,
	}
	netClient := &http.Client{
		Timeout:   time.Second * 30,
		Transport: netTransport,
	}
	resp, err := netClient.Do(request)
	if err != nil {
		return err
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			sn.log.Warn("Failed to close response body", "err", err)
		}
	}()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("failed to read response body: %w", err)
	}

	if resp.StatusCode/100 == 2 {
		var rslt map[string]interface{}
		// Slack responds to some requests with a JSON document, that might contain an error
		if err := json.Unmarshal(body, &rslt); err == nil {
			if !rslt["ok"].(bool) {
				errMsg := rslt["error"].(string)
				sn.log.Warn("Sending Slack API request failed", "url", sn.url.String(), "statusCode", resp.Status,
					"err", errMsg)
				return fmt.Errorf("failed to make Slack API request: %s", errMsg)
			}
		}

		sn.log.Debug("Sending Slack API request succeeded", "url", sn.url.String(), "statusCode", resp.Status)

		return nil
	}

	sn.log.Warn("Slack API request failed", "url", sn.url.String(), "statusCode", resp.Status, "body", string(body))
	return fmt.Errorf("request to Slack API failed with status code %d", resp.StatusCode)
}

func (sn *SlackNotifier) slackFileUpload(evalContext *alerting.EvalContext, log log.Logger, recipient, token string) error {
	if evalContext.ImageOnDiskPath == "" {
		// nolint:gosec
		// We can ignore the gosec G304 warning on this one because `setting.HomePath` comes from Grafana's configuration file.
		evalContext.ImageOnDiskPath = filepath.Join(setting.HomePath, "public/img/mixed_styles.png")
	}
	log.Info("Uploading to slack via file.upload API")
	headers, uploadBody, err := sn.generateSlackBody(evalContext.ImageOnDiskPath, token, recipient)
	if err != nil {
		return err
	}
	cmd := &models.SendWebhookSync{
		Url: "https://slack.com/api/files.upload", Body: uploadBody.String(), HttpHeader: headers, HttpMethod: "POST",
	}
	if err := bus.DispatchCtx(evalContext.Ctx, cmd); err != nil {
		log.Error("Failed to upload slack image", "error", err, "webhook", "file.upload")
		return err
	}
	return nil
}

func (sn *SlackNotifier) generateSlackBody(path string, token string, recipient string) (map[string]string, bytes.Buffer, error) {
	// Slack requires all POSTs to files.upload to present
	// an "application/x-www-form-urlencoded" encoded querystring
	// See https://api.slack.com/methods/files.upload
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	defer func() {
		if err := w.Close(); err != nil {
			// Shouldn't matter since we already close w explicitly on the non-error path
			sn.log.Warn("Failed to close multipart writer", "err", err)
		}
	}()

	// Add the generated image file
	// We can ignore the gosec G304 warning on this one because `imagePath` comes
	// from the alert `evalContext` that generates the images. `evalContext` in turn derives the root of the file
	// path from configuration variables.
	// nolint:gosec
	f, err := os.Open(path)
	if err != nil {
		return nil, b, err
	}
	defer func() {
		if err := f.Close(); err != nil {
			sn.log.Warn("Failed to close file", "path", path, "err", err)
		}
	}()
	fw, err := w.CreateFormFile("file", path)
	if err != nil {
		return nil, b, err
	}
	if _, err := io.Copy(fw, f); err != nil {
		return nil, b, err
	}
	// Add the authorization token
	if err := w.WriteField("token", token); err != nil {
		return nil, b, err
	}
	// Add the channel(s) to POST to
	if err := w.WriteField("channels", recipient); err != nil {
		return nil, b, err
	}
	if err := w.Close(); err != nil {
		return nil, b, fmt.Errorf("failed to close multipart writer: %w", err)
	}
	headers := map[string]string{
		"Content-Type":  w.FormDataContentType(),
		"Authorization": "auth_token=\"" + token + "\"",
	}
	return headers, b, nil
}
