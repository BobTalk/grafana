/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 11:25:11
 * @FilePath: /grafana/pkg/services/search/sorting.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package search

import (
	"sort"

	"github.com/grafana/grafana/pkg/services/sqlstore/searchstore"
)

var (
	SortAlphaAsc = SortOption{
		Name:        "alpha-asc",
		DisplayName: "按字母顺序（A–Z）",
		Description: "按字母顺序升序对结果进行排序",
		Index:       0,
		Filter: []SortOptionFilter{
			searchstore.TitleSorter{},
		},
	}
	SortAlphaDesc = SortOption{
		Name:        "alpha-desc",
		DisplayName: "按字母顺序（Z–A）",
		Description: "按字母顺序降序对结果进行排序",
		Index:       0,
		Filter: []SortOptionFilter{
			searchstore.TitleSorter{Descending: true},
		},
	}
)

type SortOption struct {
	Name        string
	DisplayName string
	Description string
	Index       int
	MetaName    string
	Filter      []SortOptionFilter
}

type SortOptionFilter interface {
	searchstore.FilterOrderBy
}

// RegisterSortOption allows for hooking in more search options from
// other services.
func (s *SearchService) RegisterSortOption(option SortOption) {
	s.sortOptions[option.Name] = option
}

func (s *SearchService) SortOptions() []SortOption {
	opts := make([]SortOption, 0, len(s.sortOptions))
	for _, o := range s.sortOptions {
		opts = append(opts, o)
	}
	sort.Slice(opts, func(i, j int) bool {
		return opts[i].Index < opts[j].Index || (opts[i].Index == opts[j].Index && opts[i].Name < opts[j].Name)
	})
	return opts
}
