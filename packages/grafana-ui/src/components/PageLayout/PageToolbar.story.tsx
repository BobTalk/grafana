/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-29 17:49:10
 * @FilePath: /grafana/packages/grafana-ui/src/components/PageLayout/PageToolbar.story.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React from 'react';
import { ToolbarButton, VerticalGroup } from '@grafana/ui';
import { withCenteredStory } from '../../utils/storybook/withCenteredStory';
import { PageToolbar } from './PageToolbar';
import { StoryExample } from '../../utils/storybook/StoryExample';
import { action } from '@storybook/addon-actions';
import { IconButton } from '../IconButton/IconButton';

export default {
  title: 'Layout/PageToolbar',
  component: PageToolbar,
  decorators: [withCenteredStory],
  parameters: {},
};

export const Examples = () => {
  return (
    <VerticalGroup>
      <StoryExample name="With non clickable title">
        <PageToolbar pageIcon="bell" title="Dashboard">
          <ToolbarButton icon="panel-add" />
          <ToolbarButton icon="sync">Sync</ToolbarButton>
        </PageToolbar>
      </StoryExample>
      <StoryExample name="With clickable title and parent">
        <PageToolbar
          pageIcon="apps"
          title="A very long dashboard name"
          parent="A long folder name"
          onClickTitle={() => action('Title clicked')}
          onClickParent={() => action('Parent clicked')}
          leftItems={[
            <IconButton name="share-alt" size="lg" key="share" />,
            <IconButton name="favorite" iconType="mono" size="lg" key="favorite" />,
          ]}
        >
          <ToolbarButton icon="panel-add" />
          <ToolbarButton icon="share-alt" />
          <ToolbarButton icon="sync">Sync</ToolbarButton>
          <ToolbarButton icon="cog">Settings </ToolbarButton>
        </PageToolbar>
      </StoryExample>
      <StoryExample name="Go back version">
        <PageToolbar title="服务概述 / 面板编辑" onGoBack={() => action('Go back')}>
          <ToolbarButton icon="cog" />
          <ToolbarButton icon="save" />
          <ToolbarButton>放弃</ToolbarButton>
          <ToolbarButton>应用</ToolbarButton>
        </PageToolbar>
      </StoryExample>
    </VerticalGroup>
  );
};
