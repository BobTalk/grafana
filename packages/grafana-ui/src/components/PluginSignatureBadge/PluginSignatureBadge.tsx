/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:27
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 13:54:27
 * @FilePath: /grafana/packages/grafana-ui/src/components/PluginSignatureBadge/PluginSignatureBadge.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { HTMLAttributes } from 'react';
import { PluginSignatureStatus } from '@grafana/data';
import { Badge, BadgeProps } from '../Badge/Badge';

/**
 * @public
 */
export interface PluginSignatureBadgeProps extends HTMLAttributes<HTMLDivElement> {
  status?: PluginSignatureStatus;
}

/**
 * @public
 */
export const PluginSignatureBadge: React.FC<PluginSignatureBadgeProps> = ({ status, ...otherProps }) => {
  const display = getSignatureDisplayModel(status);
  return (
    <Badge
      text={display.text}
      color={display.color as any}
      icon={display.icon}
      tooltip={display.tooltip}
      {...otherProps}
    />
  );
};

PluginSignatureBadge.displayName = 'PluginSignatureBadge';

function getSignatureDisplayModel(signature?: PluginSignatureStatus): BadgeProps {
  if (!signature) {
    signature = PluginSignatureStatus.invalid;
  }

  switch (signature) {
    case PluginSignatureStatus.internal:
      return { text: '核心', color: 'blue', tooltip: '与Grafana捆绑的核心插件' };
    case PluginSignatureStatus.valid:
      return { text: '签名', icon: 'lock', color: 'green', tooltip: '已签名和验证的插件' };
    case PluginSignatureStatus.invalid:
      return {
        text: '无效的签名',
        icon: 'exclamation-triangle',
        color: 'red',
        tooltip: '插件签名无效',
      };
    case PluginSignatureStatus.modified:
      return {
        text: '修改后的签名',
        icon: 'exclamation-triangle',
        color: 'red',
        tooltip: '有效签名，但内容已修改',
      };
    case PluginSignatureStatus.missing:
      return {
        text: '缺少签名',
        icon: 'exclamation-triangle',
        color: 'red',
        tooltip: '缺少签名插件',
      };
    default:
      return {
        text: '未签名',
        icon: 'exclamation-triangle',
        color: 'red',
        tooltip: '未签名的外部插件',
      };
  }
}
