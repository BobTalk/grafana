/*
 * @Author: heyongqiang 1498833800@qq.com
 * @Date: 2023-05-28 10:53:26
 * @LastEditors: heyongqiang 1498833800@qq.com
 * @LastEditTime: 2023-05-30 13:29:06
 * @FilePath: /grafana/packages/grafana-ui/src/components/Collapse/CollapsableSection.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { FC, ReactNode, useState } from 'react';
import { css } from '@emotion/css';
import { useStyles2 } from '../../themes';
import { Icon } from '..';
import { GrafanaTheme2 } from '@grafana/data';

export interface Props {
  label: string;
  isOpen: boolean;
  children: ReactNode;
}

export const CollapsableSection: FC<Props> = ({ label, isOpen, children }) => {
  const [open, toggleOpen] = useState<boolean>(isOpen);
  const styles = useStyles2(collapsableSectionStyles);
  const headerStyle = open ? styles.header : styles.headerCollapsed;
  const tooltip = `点击${open ? '折叠' : '展开'}`;

  return (
    <div>
      <div onClick={() => toggleOpen(!open)} className={headerStyle} title={tooltip}>
        {label}
        <Icon name={open ? 'angle-down' : 'angle-right'} size="xl" className={styles.icon} />
      </div>
      {open && <div className={styles.content}>{children}</div>}
    </div>
  );
};

const collapsableSectionStyles = (theme: GrafanaTheme2) => {
  const header = css({
    display: 'flex',
    justifyContent: 'space-between',
    fontSize: theme.typography.size.lg,
    padding: `${theme.spacing(0.5)} 0`,
    cursor: 'pointer',
  });
  const headerCollapsed = css(header, {
    borderBottom: `1px solid ${theme.colors.border.weak}`,
  });
  const icon = css({
    color: theme.colors.text.secondary,
  });
  const content = css({
    padding: `${theme.spacing(2)} 0`,
  });

  return { header, headerCollapsed, icon, content };
};
