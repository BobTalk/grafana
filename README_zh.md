[源仓库](https://github.com/grafana/grafana.git)

[现仓库](https://gitlab.com/BobTalk/grafana.git)

1. git pull origin main
2. git reset --hard
3. yarn install
4. yarn start
5. make run
6. 打包文件在public中 替换对应镜像文件即可
7. sudo docker build -t grafana:v8.0.6 ./
